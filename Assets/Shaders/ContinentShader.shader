﻿Shader "Unlit/ContinentShader"
{
    Properties
    {
        _MapTex ("Map Texture", 2D) = "white" {}
        
        _GrassColor("Grass Colour", Color) = (1, 1, 1, 1)
        _GrassTexture("Grass Texture", 2D) = "white" {}

        _SnowColor("Snow Colour", Color) = (1, 1, 1, 1)
        _SnowTexture("Snow Texture", 2D) = "white" {}

        _SandColor("Sand Colour", Color) = (1, 1, 1, 1)
        _SandTexture("Sand Texture", 2D) = "white" {}

        _DirtColor("Dirt Colour", Color) = (1, 1, 1, 1)
        _DirtTexture("Dirt Texture", 2D) = "white" {}

        _ForestColor("Forest Colour", Color) = (1, 1, 1, 1)
        _ForestColorMod("Forest Colour Modifier", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue" = "Background+400"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;

                float2 grasUv : TEXCOORD1;
                float2 snowUv : TEXCOORD2;
                float2 sandUv : TEXCOORD3;
                float2 dirtUv : TEXCOORD4;
            };

            sampler2D _MapTex;
            float4 _MapTex_TexelSize;

            sampler2D _GrassTexture;
            float4 _GrassTexture_ST;
            sampler2D _SnowTexture;
            float4 _SnowTexture_ST;
            sampler2D _SandTexture;
            float4 _SandTexture_ST;
            sampler2D _DirtTexture;
            float4 _DirtTexture_ST;

            fixed4 _GrassColor;
            fixed4 _SnowColor;
            fixed4 _SandColor;
            fixed4 _DirtColor;
            fixed4 _ForestColor;
            fixed4 _ForestColorMod;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                o.grasUv = TRANSFORM_TEX(v.uv, _GrassTexture);
                o.snowUv = TRANSFORM_TEX(v.uv, _SnowTexture);
                o.sandUv = TRANSFORM_TEX(v.uv, _SandTexture);
                o.dirtUv = TRANSFORM_TEX(v.uv, _DirtTexture);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 mapSample = tex2D(_MapTex, i.uv);
                fixed4 color = 1;

                if (mapSample.a < 1)
                {
                    discard;
                }
                else
                {
                    fixed4 grassColor = tex2D(_GrassTexture, i.grasUv);
                    if (all(mapSample.rgb == _GrassColor.rgb))
                    {
                        color = grassColor;
                    }
                    else
                    {
                        int snow = all(mapSample == _SnowColor);
                        int sand = all(mapSample == _SandColor);
                        int dirt = all(mapSample == _DirtColor);
                        int forest = all(mapSample.rgb == _ForestColor.rgb);

                        fixed4 snowColor = tex2D(_SnowTexture, i.snowUv);
                        fixed4 sandColor = tex2D(_SandTexture, i.sandUv);
                        fixed4 dirtColor = tex2D(_DirtTexture, i.dirtUv);

                        color = snow * snowColor + sand * sandColor + dirt * dirtColor + forest * grassColor * _ForestColorMod;

                        if (!(snow || sand || dirt || forest))
                        {
                            float dGrass = length(abs(mapSample.rgb - _GrassColor.rgb));
                            float dSnow = length(abs(mapSample.rgb - _SnowColor.rgb));
                            float dSand = length(abs(mapSample.rgb - _SandColor.rgb));
                            float dDirt = length(abs(mapSample.rgb - _DirtColor.rgb));
                            float dForest = length(abs(mapSample.rgb - _ForestColor.rgb));

                            float smallest = min(dGrass, min(dSnow, min(dSand, min(dDirt, dForest))));

                            if (smallest == dSnow)
                            {
                                color = lerp(snowColor, grassColor, dSnow);
                            }
                            else if (smallest == dSand)
                            {
                                color = lerp(sandColor, grassColor, dSand);
                            }
                            else if (smallest == dDirt)
                            {
                                color = lerp(dirtColor, grassColor, dDirt);
                            }
                            else
                            {
                                color = grassColor;
                                if (smallest == dForest)
                                {
                                    color *= lerp(_ForestColorMod, 1, dForest);
                                }
                            }
                        }
                    }                    
                }
                
                return color;
            }
            ENDCG
        }
    }
}
