﻿Shader "Unlit/RoadShader"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _AlphaCutoff("Alpha Cutoff", Range(0,1)) = 0

        _MapText("Map Texture", 2D) = "white" {}
        _MapSize("Map Quad Scale", Vector) = (0, 0, 0, 0)
        _MapPos("Map Quad Position", Vector) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Geometry" "IgnoreProjector" = "True"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 world : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _MapText;
            float4 _MainTex_ST;
            float4 _MapTex_TexelSize;
            float4 _MapSize;
            float4 _MapPos;
            half _AlphaCutoff;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                o.world = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

                float2 pointRelativeTo0 = float2(i.world.x, i.world.z);
                pointRelativeTo0 -= (float2(_MapPos.x - _MapSize.x / 2, _MapPos.z - _MapSize.y / 2));

                float2 localUv = float2(pointRelativeTo0.x / _MapSize.x, pointRelativeTo0.y / _MapSize.y);
                fixed4 mapCol = tex2D(_MapText, localUv);

                col *= mapCol.a;

                if (col.a < _AlphaCutoff)
                {
                    discard;
                }

                return col;
            }
            ENDCG
        }
    }
}
