﻿Shader "Unlit/FadeTowardsCenter"
{
    Properties
    {
        _FadeColor("Fade Colour", Color) = (1, 1, 1, 1)
        _CenterX("CenterX", Range(0, 1)) = 0.5
        _CenterY("CenterY", Range(0, 1)) = 0.5
        _Size("Size", Range(0, 1)) = 0.25
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed4 _FadeColor;
            float _CenterX;
            float _CenterY;
            float _Size;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float a = 1 - clamp(length(float2(_CenterX, _CenterY) - i.uv) - _Size, 0, 1);

                fixed4 col = _FadeColor;
                col.a = a * a * a * a;

                return col;
            }
            ENDCG
        }
    }
}
