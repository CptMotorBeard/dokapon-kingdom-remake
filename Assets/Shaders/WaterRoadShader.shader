﻿Shader "Unlit/WaterRoadShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _AlphaCutoff("Alpha Cutoff", Range(0,1)) = 0
        _RippleSpeed("Ripple Speed", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Background+200" "IgnoreProjector" = "True"}
        LOD 100
        
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            half _AlphaCutoff;
            float _RippleSpeed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 uv = i.uv;
                uv.y += _Time.y / _RippleSpeed;

                fixed4 col = tex2D(_MainTex, uv);
                if (col.a < _AlphaCutoff)
                {
                    discard;
                }

                return col;
            }
            ENDCG
        }
    }
}
