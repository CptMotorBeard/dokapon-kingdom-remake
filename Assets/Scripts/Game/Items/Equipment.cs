﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

public class Equipment : Item
{
    public int Id;

    public int Attack;
    public int Defense;
    public int Magic;
    public int Speed;
    public int Hp;

    public override ItemType Type => throw new System.NotImplementedException();
}
