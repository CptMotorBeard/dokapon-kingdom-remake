﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public enum ItemType
{
    Accessory,
    Weapon,
    Shield,
    DefMagic,
    OffMagic,
    FieldMagic,
    Item
}

public abstract class Item : ScriptableObject
{
    public abstract ItemType Type { get; }

    public string Name;
    public Sprite ItemSprite;
    public uint Price;

    [TextArea(4, 4)]
    public string Description;

    public virtual Accessory GetAsAccessory() { return null; }
    public virtual Weapon GetAsWeapon() { return null; }
    public virtual Shield GetAsShield() { return null; }
    public virtual BaseDefenseMagic GetAsDefMagic() { return null; }
    public virtual BaseMagic GetAsOffMagic() { return null; }
    public virtual Consumable GetAsConsumable() { return null; }

    public bool IsEquipment => (Type == ItemType.Accessory || Type == ItemType.DefMagic || Type == ItemType.OffMagic || Type == ItemType.Shield || Type == ItemType.Weapon);
}

[System.Serializable]
public class StatValuePair
{
    public CharacterStat Stat;
    public int Value;
}