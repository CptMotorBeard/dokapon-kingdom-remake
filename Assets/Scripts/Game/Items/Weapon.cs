﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Game/Equipment/Weapon")]
public class Weapon : Equipment
{
    // public PlayerJob PreferredJob;
    public override ItemType Type => ItemType.Weapon;

    public override Weapon GetAsWeapon()
    {
        return this;
    }
}
