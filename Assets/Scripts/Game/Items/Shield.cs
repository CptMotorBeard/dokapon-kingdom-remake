﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Shield", menuName = "Game/Equipment/Shield")]
public class Shield : Equipment
{
    public override ItemType Type => ItemType.Shield;

    public override Shield GetAsShield()
    {
        return this;
    }
}
