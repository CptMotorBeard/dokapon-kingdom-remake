﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Accessory", menuName = "Game/Equipment/Accessory")]
public class Accessory : Equipment
{
    public override ItemType Type => ItemType.Accessory;

    public override Accessory GetAsAccessory()
    {
        return this;
    }
}