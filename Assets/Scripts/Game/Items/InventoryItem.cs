﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Game/Items/New Item")]
public class InventoryItem : Item
{
    public override ItemType Type => ItemType.Item;
}
