﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

public abstract class Consumable : InventoryItem
{
    public void UseItem(Player player)
    {
        player.RemoveItem(this);
        OnConsume(player);
    }

    protected abstract void OnConsume(Player player);

    public override Consumable GetAsConsumable()
    {
        return this;
    }
}
