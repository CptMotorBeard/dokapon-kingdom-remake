﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Healing Item", menuName = "Game/Items/New Healing Item")]
public class HealingItem : Consumable
{
    public int HealPercent;
    // TODO -- Status effects

    protected override void OnConsume(Player player)
    {
        int amount = (int)(player.CharacterStats[CharacterStat.Hp] * (HealPercent / 100f));
        player.Heal(amount);
    }
}
