﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Item Spinner", menuName = "Game/Items/New Item Spinner")]
public class SpinnerItem : Consumable
{
    public int NumberOfSpinners;

    protected override void OnConsume(Player player)
    {
        player.NumberOfSpinners = NumberOfSpinners;
    }
}
