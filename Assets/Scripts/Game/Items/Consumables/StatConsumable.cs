﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Stat Item", menuName = "Game/Items/Stat Item")]
public class StatConsumable : Consumable
{
    public StatValuePair[] Stats;
    public int Duration;
    public Character.StatModifier.ModifierDurationType DurationType;

    protected override void OnConsume(Player player)
    {
        foreach (var stat in Stats)
        {
            Character.StatModifier mod = new Character.StatModifier(DurationType, Duration, stat.Value);
            player.AddStatModifier(stat.Stat, mod);
        }
    }
}
