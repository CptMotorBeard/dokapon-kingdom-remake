﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Item Crystal", menuName = "Game/Items/New Item Crystal")]
public class ItemCrystal : Consumable
{
    public int CrystalValue = 0;

    protected override void OnConsume(Player player)
    {
        player.ForcedMovementSpaces = CrystalValue;
    }
}
