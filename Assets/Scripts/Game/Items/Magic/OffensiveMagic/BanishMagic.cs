﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Banish Magic")]
public class BanishMagic : BaseMagic
{
    // Kills the opponent but leaves the caster at 1 HP.
    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        caster.TakeDamage(caster.CurHp - 1);
        opponent.TakeDamage(opponent.CurHp);
    }
}
