﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Break Magic")]
public class BreakMagic : BaseMagic
{
    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        /* TODO break opponent gear if blocked
         * 
         * int possibleGear = opponent.Player.Gear.Count;
         * int brokenGear = Random.Range(0, possibleGear);
         * opponent.Player.Gear[brokenGear] = null;
         * 
         */
    }
}
