﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Base Magic")]
public class BaseMagic : Item
{
    public enum Element
    {
        Fire,
        Lightning,
        Ice,
        Wind,
        Physical,
        Light,
        Dark,
        Drain
    }

    public override ItemType Type => ItemType.OffMagic;

    public int Id;
    public float Power;
    public Element MagicType;

    public virtual void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        return;
    }

    public override BaseMagic GetAsOffMagic()
    {
        return this;
    }
}
