﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Stats Down Magic")]
public class ReduceStatsMagic : BaseMagic
{
    public StatValuePair[] StatsToDecrease;
    public int Duration;
    public Character.StatModifier.ModifierDurationType DurationType;

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        foreach (var stat in StatsToDecrease)
        {
            var mod = new Character.StatModifier(DurationType, Duration, stat.Value);
            opponent.CharacterBackend.AddStatModifier(stat.Stat, mod);
        }
    }
}
