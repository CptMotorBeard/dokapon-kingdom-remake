﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Status Magic")]
public class StatusEffectMagic : BaseMagic
{
    // Public StatusEffect EffectToApply

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        /* TODO Apply status effect
         * 
         * opponent.ApplyStatusEffect(EffectToApply);
         * 
        */
    }
}