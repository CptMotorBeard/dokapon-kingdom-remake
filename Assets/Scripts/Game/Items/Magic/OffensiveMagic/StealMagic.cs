﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Steal Magic")]
public class StealMagic : BaseMagic
{
    public int StealAmount;

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        // TODO - Deal with monster values

        long amount = 0;
        if (opponent.CharacterBackend is Player)
        {
            Player p = opponent.CharacterBackend as Player;
            amount = (long)(p.TotalGold * (StealAmount / 100f));
            p.AddGold(-amount);
        }
        else
        {
            
        }

        if (caster.CharacterBackend is Player)
        {
            Player p = caster.CharacterBackend as Player;
            p.AddGold(amount);
        }
        else
        {

        }

        // TODO - Add popup
    }
}
