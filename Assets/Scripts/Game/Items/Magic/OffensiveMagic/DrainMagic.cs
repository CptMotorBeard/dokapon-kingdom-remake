﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Drain Magic")]
public class DrainMagic : BaseMagic
{
    public int DrainAmount;

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        float amount = opponent.CurHp * (DrainAmount / 100f);

        opponent.TakeDamage(amount);
        caster.TakeDamage(-amount);
    }
}
