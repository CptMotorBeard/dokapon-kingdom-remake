﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Swap Magic")]
public class SwapMagic : BaseMagic
{
    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        int amount = opponent.CurHp - caster.CurHp;

        caster.TakeDamage(-amount);
        opponent.TakeDamage(amount);
    }
}