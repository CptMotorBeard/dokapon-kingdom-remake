﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Buff Defensive Magic")]
public class BuffDefensiveMagic : BaseDefenseMagic
{
    public StatValuePair[] StatsToBuff;
    public int Duration;
    public Character.StatModifier.ModifierDurationType DurationType;

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        foreach (var stat in StatsToBuff)
        {
            int statMod = (int)((stat.Value / 100f) * caster.CharacterBackend.CharacterStats[stat.Stat]);
            var mod = new Character.StatModifier(DurationType, Duration, statMod);
            caster.CharacterBackend.AddStatModifier(stat.Stat, mod);
        }
    }
}
