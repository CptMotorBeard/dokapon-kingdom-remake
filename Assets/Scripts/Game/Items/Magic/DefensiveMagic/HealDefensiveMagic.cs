﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Heal Defense Magic")]
public class HealDefensiveMagic : BaseDefenseMagic
{
    public int HealAmount;
    // public StatusEffect[] CuredStatusEffects;

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        float amount = caster.MaxHp * (HealAmount / 100f);
        caster.TakeDamage(-amount);

        /* TODO cure status
         * 
         * foreach (StatusEffect statusEffect in CuredStatusEffects)
         * {
         *      caster.CureStatusEffect(statusEffect);
         * }
         * 
         */
    }
}
