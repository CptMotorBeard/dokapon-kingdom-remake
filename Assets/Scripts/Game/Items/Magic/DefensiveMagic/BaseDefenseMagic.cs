﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Defensive Magic")]
public class BaseDefenseMagic : Item
{
    public enum ProtectionType
    {
        Protect,
        Heal,
        Counter,
        Assist
    }

    public override ItemType Type => ItemType.DefMagic;

    public int Id;
    public int Power;
    public ProtectionType Protection;

    public virtual void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        return;
    }    

    public override BaseDefenseMagic GetAsDefMagic()
    {
        return this;
    }
}
