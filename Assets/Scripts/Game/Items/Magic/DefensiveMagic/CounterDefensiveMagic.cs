﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Magic", menuName = "Game/Equipment/Magic/Counter Defensive Magic")]
public class CounterDefensiveMagic : BaseDefenseMagic
{
    public CombatMath.AbilityOptions[] ActionsSealed;
    // public StatusEffect EffectToApply;
    public int DamageReflected;
    public bool BounceEffect;
    public bool ReflectStatus;

    public override void OnCast(CombatCharacter caster, CombatCharacter opponent)
    {
        /* TODO everything here
         * 
         * opponent.SealActions(ActionsSealed);
         * opponent.ApplyStatusEffect(EffectToApply);
         * 
         * float damageTaken = caster.GetDamageTaken();
         * 
         * if (BounceEffect)
         * {
         *      caster.TakeDamage(-damageTaken);
         * }
         * 
         * damageTaken *= (DamageReflected / 100f);
         * opponent.TakeDamage(damageTaken);
         * 
         * if (ReflectStatus)
         * {
         *      StatusEffect[] newEffects = caster.GetRecentStatusEffects();
         *      caster.ClearRecentStatusEffects();
         *      opponent.ApplyStatusEffects(newEffects);
         * }
         */
    }
}
