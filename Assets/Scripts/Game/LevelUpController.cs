﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using TMPro;
using System;
using System.Collections.Generic;

public class LevelUpController : MonoBehaviour
{
    public TextMeshProUGUI JobName;
    public TextMeshProUGUI CharacterName;
    public TextMeshProUGUI CharacterLevel;
    public TextMeshProUGUI RemainingAbilityPoints;

    public LevelUpStatController AT;
    public LevelUpStatController DF;
    public LevelUpStatController MG;
    public LevelUpStatController SP;
    public LevelUpStatController HP;

    public GameObject SkillPointsObject;
    public GameObject ExitButton;

    Dictionary<CharacterStat, LevelUpStatController> mControllers;
    Dictionary<CharacterStat, int> mLevelUpBonus;
    int mRemainingSkillPoints;
    int mTimesLeveled;
    bool bCanIncreaseStats = false;

    public void Initialize(int numTimes)
    {
        mLevelUpBonus = new Dictionary<CharacterStat, int>();
        mControllers = new Dictionary<CharacterStat, LevelUpStatController>()
        {
            { CharacterStat.Attack, AT },
            { CharacterStat.Defense, DF },
            { CharacterStat.Magic, MG },
            { CharacterStat.Speed, SP },
            { CharacterStat.Hp, HP }
        };

        Player levelPlayer = GlobalGameManager.Instance.GetCurrentPlayer();
        var bonuses = levelPlayer.GetLevelUpBonus();

        mTimesLeveled = numTimes;
        mRemainingSkillPoints = PlayerUtils.SkillPointsPerLevel * mTimesLeveled;

        JobName.text = levelPlayer.Job.JobName;
        CharacterName.text = levelPlayer.CharacterName;
        CharacterLevel.text = $"{levelPlayer.Level + mTimesLeveled}";
        RemainingAbilityPoints.text = $"{mRemainingSkillPoints}";

        AT.OnAnimationComplete += LevelUpAnimationComplete;
        DF.OnAnimationComplete += LevelUpAnimationComplete;
        MG.OnAnimationComplete += LevelUpAnimationComplete;
        SP.OnAnimationComplete += LevelUpAnimationComplete;
        HP.OnAnimationComplete += LevelUpAnimationComplete;

        foreach (var stat in (CharacterStat[])Enum.GetValues(typeof(CharacterStat)))
        {
            if (bonuses.ContainsKey(stat))
            {
                mControllers[stat].Initialize(levelPlayer.CharacterStats[stat], bonuses[stat] * mTimesLeveled * (stat == CharacterStat.Hp ? 10 : 1));
            }
            else
            {
                mControllers[stat].Initialize(levelPlayer.CharacterStats[stat], 0);
            }
        }
    }

    private int mNumAnimationsFinished = 0;
    public void LevelUpAnimationComplete()
    {
        ++mNumAnimationsFinished;

        if (mNumAnimationsFinished >= 5)
        {
            foreach (var item in mControllers)
            {
                item.Value.AllAnimationsComplete();
            }

            SkillPointsObject.SetActive(true);
            bCanIncreaseStats = true;
        }
    }

    public void IncreaseStat(int statToIncreaseAsInt)
    {
        if (!bCanIncreaseStats || statToIncreaseAsInt < 0 || statToIncreaseAsInt >= Enum.GetNames(typeof(CharacterStat)).Length)
        {
            return;
        }

        CharacterStat statToIncrease = (CharacterStat)statToIncreaseAsInt;

        if (mLevelUpBonus.ContainsKey(statToIncrease))
        {
            ++mLevelUpBonus[statToIncrease];
        }
        else
        {
            mLevelUpBonus.Add(statToIncrease, 1);
        }

        int bonus = 1;
        if (statToIncrease == CharacterStat.Hp)
            bonus *= 10;

        mControllers[statToIncrease].IncreaseStat(bonus);

        mRemainingSkillPoints -= 1;
        RemainingAbilityPoints.text = $"{mRemainingSkillPoints}";

        if (mRemainingSkillPoints <= 0)
        {
            GlobalGameManager.Instance.GetCurrentPlayer().LevelUp(mLevelUpBonus, mTimesLeveled);
            ExitButton.SetActive(true);
            bCanIncreaseStats = false;
        }
    }

    private void OnDestroy()
    {
        AT.OnAnimationComplete -= LevelUpAnimationComplete;
        DF.OnAnimationComplete -= LevelUpAnimationComplete;
        MG.OnAnimationComplete -= LevelUpAnimationComplete;
        SP.OnAnimationComplete -= LevelUpAnimationComplete;
        HP.OnAnimationComplete -= LevelUpAnimationComplete;
    }
}
