﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public static class DelegateUtils
{
    public delegate void AnimationCompleteDelegate();
}

public static class PlayerUtils
{
    public const int SkillPointsPerLevel = 2;

    public static ulong GetRequiredXpByLevel(int level)
    {
        ulong xpRequired = (ulong)Mathf.Pow(8, Mathf.Log(level, 2));

        return xpRequired;
    }
}