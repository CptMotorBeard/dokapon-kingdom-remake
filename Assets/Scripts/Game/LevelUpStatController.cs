﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using TMPro;

public class LevelUpStatController : MonoBehaviour
{    
    public DelegateUtils.AnimationCompleteDelegate OnAnimationComplete;

    public Color BaseTextColor;

    public TextMeshProUGUI StatText;
    public TextMeshProUGUI LevelUpAmount;
    public TextMeshProUGUI LevelUpAmountCopy;

    public GameObject LevelUpArrow;
    public Animator LevelUpAnimator;

    private int mLevelUpBonus;
    private int mStatValue;
    private int mTotalStatBonus;

    public void Initialize(int statValue, int levelUpBonus)
    {
        StatText.text = $"{statValue}";
        
        mTotalStatBonus = 0;
        mLevelUpBonus = levelUpBonus;
        mStatValue = statValue;

        if (mLevelUpBonus <= 0)
        {
            LevelUpAmount.text = "";
            LevelUpAmountCopy.text = "";
            LevelUpArrow.SetActive(false);

            OnAnimationComplete?.Invoke();
        }
        else
        {
            LevelUpAmount.text = $"{mLevelUpBonus}";
            LevelUpAmountCopy.text = $"{mLevelUpBonus}";

            LevelUpAnimator.SetTrigger("LevelUp");
        }
    }

    public void IncreaseStat(int amount)
    {
        mTotalStatBonus += amount;
        StatText.text = $"{mStatValue + mLevelUpBonus + mTotalStatBonus}";
    }

    public void UpdateStatText()
    {
        StatText.text = $"{mStatValue + mLevelUpBonus}";

        Debug.Log("Change Value");
    }

    public void AnimationComplete()
    {
        OnAnimationComplete?.Invoke();

        Debug.Log("Animation Complete");
    }

    public void AllAnimationsComplete()
    {
        StatText.color = BaseTextColor;

        LevelUpAmount.text = "";
        LevelUpAmountCopy.text = "";
        LevelUpArrow.SetActive(false);
    }
}
