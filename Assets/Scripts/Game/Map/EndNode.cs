﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using UnityEngine;

public class EndNode : MonoBehaviour
{
    protected GlobalGameManager mGlobalGameManager;
    public void Start()
    {
        mGlobalGameManager = GlobalGameManager.Instance;
    }

    /*
     * When overriding LandedOnNode, mGlobalGameManager.TurnComplete(); must be called before returning to the board.
     * For nodes that switch scenes, this can be part of returning back to the board scene.
     */
    public virtual IEnumerator LandedOnNode(PlayerController[] players, PlayerUi ui, RoutingNode endNode)
    {
        mGlobalGameManager.TurnComplete();
        yield return 0;
    }

    public void SaveBoardState(PlayerController[] players)
    {
        PlayerMapData[] mapData = new PlayerMapData[players.Length];

        for (int i = 0; i < players.Length; i++)
        {
            PlayerMapData d = new PlayerMapData
            {
                Location = players[i].transform.position
            };

            mapData[i] = d;
        }

        mGlobalGameManager.SaveBoardState(mapData);
    }
}
