﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using UnityEngine;

public class TownNode : BattleNode
{
    public TownStats TownInformation;
    public TownDisplayPanel DisplayPanel;

    public GameObject Boss;
    
    public void Initialize(Character boss)
    {
        mEnemyCharacter = boss;
    }

    public void Initialize()
    {
        mEnemyCharacter = mGlobalGameManager.GetTownInformation(TownInformation).BossMonster;

        if (mEnemyCharacter == null)
        {
            Boss.SetActive(false);
        }
    }

    public override IEnumerator LandedOnNode(PlayerController[] players, PlayerUi ui, RoutingNode endNode)
    {        
        if (mEnemyCharacter == null)
        {
            SaveBoardState(players);

            Town thisTown = mGlobalGameManager.GetTownInformation(TownInformation);
            mGlobalGameManager.SwitchToTownScene(thisTown);

            yield return new WaitForSeconds(GlobalGameManager.kTransitionDelay);
        }
        else
        {
            yield return base.LandedOnNode(players, ui, endNode);
        }
    }
}
