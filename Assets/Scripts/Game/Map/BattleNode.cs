﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class PossibleSpawn //-V3097
{
    public int Weight;
    public EnemyStats Enemy;
}

public class BattleNode : EndNode
{
    public List<PossibleSpawn> AllSpawns;

    protected Character mEnemyCharacter;
    Dictionary<int, Character> mPvpFight;

    public new void Start()
    {
        base.Start();

        mEnemyCharacter = null;
        mPvpFight = new Dictionary<int, Character>();
    }

    public EnemyStats GetEnemy()
    {
        int totalWeight = AllSpawns.Sum((i) => { return i.Weight; });
        int r = Random.Range(1, totalWeight + 1);

        int currentWeight = 0;
        foreach (var enemy in AllSpawns)
        {
            currentWeight += enemy.Weight;
            if (currentWeight >= r)
            {
                return enemy.Enemy;
            }
        }

        return AllSpawns[0].Enemy;
    }

    public override IEnumerator LandedOnNode(PlayerController[] players, PlayerUi ui, RoutingNode endNode)
    {
        mGlobalGameManager.OnBattleStart += mGlobalGameManager.FlowManager.StartBattle;

        bool pvp = false;
        int pvpIndex = -1;
        
        if (mPvpFight.Count > 0)
        {
            pvp = true;
            pvpIndex = mPvpFight.First().Key;
        }

        SaveBoardState(players);
        
        if (pvp)
        {
            yield return ui.StartBattle(pvpIndex);
            mGlobalGameManager.SwitchToBattleScene(pvpIndex);
        }
        else
        {
            if (mEnemyCharacter != null)
            {
                yield return ui.StartBattle(mEnemyCharacter);
                mGlobalGameManager.SwitchToBattleScene(mEnemyCharacter);
            }
            else
            {
                EnemyStats enemy = GetEnemy();
                yield return ui.StartBattle(enemy);
                mGlobalGameManager.SwitchToBattleScene(enemy);
            }            
        }

        yield return new WaitForSeconds(GlobalGameManager.kTransitionDelay);
    }

    public void AddPvpFight(int index)
    {
        mPvpFight.Add(index, mGlobalGameManager.GetPlayer(index));
    }

    public void ClearPvP()
    {
        mPvpFight.Clear();
    }

    public void AddCharacter(Character character)
    {
        mEnemyCharacter = character;
    }
}
