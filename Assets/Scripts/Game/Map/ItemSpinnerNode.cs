﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemSpinnerNode : EndNode
{
    public List<DropListItem> AvailableItems;

    public override IEnumerator LandedOnNode(PlayerController[] players, PlayerUi ui, RoutingNode endNode)
    {
        Item item = null;
        int totalWeight = AvailableItems.Sum((i) => { return i.Weight; });
        int r = Random.Range(1, totalWeight + 1);

        int currentWeight = 0;
        foreach (DropListItem dropListItem in AvailableItems)
        {
            currentWeight += dropListItem.Weight;
            if (currentWeight >= r)
            {
                item = dropListItem.DropItem;
                break;
            }
        }

        if (item == null)
        {
            Debug.LogError("Don't know how, but an item failed to drop");
            yield return 0;
        }

        Item tooManyItems = mGlobalGameManager.GetCurrentPlayer()?.AddItem(item);
        List<Item> items = AvailableItems.Select((i) => i.DropItem).ToList();
        yield return ui.StartItemSpinner(items, item);

        if (tooManyItems != null)
        {
            /*
             * 
             * yield return ui.TooManyItemsUi.ForceDrop(1);
             * 
             */
        }

        mGlobalGameManager.TurnComplete();
    }
}
