﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopNode : EndNode
{
    public List<Item> ShopItems;
    public ShopType Shop;

    public override IEnumerator LandedOnNode(PlayerController[] players, PlayerUi ui, RoutingNode endNode)
    {
        SaveBoardState(players);
        mGlobalGameManager.SwitchToShopScene(ShopItems, Shop);

        yield return new WaitForSeconds(GlobalGameManager.kTransitionDelay);
    }
}
