﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;

public class DEBUG_Castle : EndNode
{
    public override IEnumerator LandedOnNode(PlayerController[] players, PlayerUi ui, RoutingNode endNode)
    {
        var p = mGlobalGameManager.GetCurrentPlayer();
        p.Heal(p.CharacterStats[CharacterStat.Hp]);
        mGlobalGameManager.TurnComplete();
        yield return null;
    }
}
