﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public class BattleNodeManager
{
    Dictionary<Vector3, Character> mBattleNodeStatus;

    public BattleNodeManager()
    {
        mBattleNodeStatus = new Dictionary<Vector3, Character>();
    }

    public Character CharactersOnNode(Vector3 position)
    {
        if (mBattleNodeStatus.ContainsKey(position))
        {
            return mBattleNodeStatus[position];
        }

        return null;
    }

    public void BattleEnd(Vector3 position, Character enemy)
    {
        if (enemy.CurHp <= 0)
        {
            if (mBattleNodeStatus.ContainsKey(position))
            {
                mBattleNodeStatus.Remove(position);
            }
        }
        else
        {
            if (mBattleNodeStatus.ContainsKey(position))
            {
                mBattleNodeStatus[position] = enemy;
            }
            else
            {
                mBattleNodeStatus.Add(position, enemy);
            }
        }        
    }
}
