﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment
{
    private Dictionary<ItemType, Item> mEquipment;
    public IReadOnlyDictionary<ItemType, Item> Equipment => mEquipment;
    
    public PlayerEquipment()
    {
        mEquipment = new Dictionary<ItemType, Item>()
        {
            { ItemType.Weapon, null },
            { ItemType.Shield, null },
            { ItemType.Accessory, null },
            { ItemType.OffMagic, null },
            { ItemType.DefMagic, null }
        };
    }

    public bool RequiresSwap(Item e)
    {
        if (mEquipment.ContainsKey(e.Type))
        {
            return mEquipment[e.Type] != null;
        }

        Debug.LogError("Trying to equip non equipment");
        return false;
    }

    public Item Equip(Item e)
    {
        Item swappedEquipment = null;
        if (mEquipment.ContainsKey(e.Type))
        {
            swappedEquipment = mEquipment[e.Type];
            mEquipment[e.Type] = e;
        }
        else
        {
            Debug.LogError("Trying to equip non equipment");
        }

        return swappedEquipment;
    }

    public Item Unequip(ItemType type)
    {
        Item swappedEquipment = null;
        if (mEquipment.ContainsKey(type))
        {
            swappedEquipment = mEquipment[type];
            mEquipment[type] = null;
        }
        else
        {
            Debug.LogError("Trying to unequip non equipment");
        }

        return swappedEquipment;
    }
}

public class PlayerInventory
{
    private int mMaxInventorySize;
    private List<Item> mItems;
    public IReadOnlyList<Item> Items => mItems;

    public PlayerInventory(int inventorySize)
    {
        mMaxInventorySize = inventorySize;
        mItems = new List<Item>();
    }

    public List<Item> AddItems(List<Item> itemsToAdd)
    {
        List<Item> itemsNotAdded = new List<Item>();

        foreach (Item item in itemsToAdd)
        {
            if (mItems.Count >= mMaxInventorySize)
            {
                itemsNotAdded.Add(item);
            }
            else
            {
                mItems.Add(item);
            }
        }

        return itemsNotAdded;
    }

    public Item AddItem(Item itemToAdd)
    {
        if (mItems.Count >= mMaxInventorySize)
        {
            return itemToAdd;
        }

        mItems.Add(itemToAdd);
        return null;
    }

    public void Clear()
    {
        mItems.Clear();
    }

    public List<Item> ResizeInventory(int newSize)
    {
        List<Item> items = mItems;

        mItems.Clear();
        mMaxInventorySize = newSize;

        return AddItems(items);
    }

    public bool RemoveItem(Item item)
    {
        if (mItems.Contains(item))
        {
            mItems.Remove(item);
            return true;
        }

        return false;
    }
}
