﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using System.Linq;
public enum CharacterStat
{
    Hp,
    Attack,
    Defense,
    Magic,
    Speed
}

public class Character
{
    public class StatModifier
    {
        public enum ModifierDurationType
        {
            Turns,
            Combats,
            Days,
            Weeks,
            Permanent
        }

        public int Duration;
        public ModifierDurationType DurationType;
        public int Modifier;

        public string Id;

        public StatModifier(ModifierDurationType durationType, int duration, int modifier)
        {
            Duration = duration;
            DurationType = durationType;
            Modifier = modifier;
        }
    }

    protected Dictionary<CharacterStat, List<StatModifier>> mCharacterStatModifier;

    protected Dictionary<CharacterStat, int> mBaseStats;

    public CharacterStats BaseCharacter { get; protected set; }
    public IReadOnlyDictionary<CharacterStat, int> CharacterStats
    {
        get
        {
            Dictionary<CharacterStat, int> total = new Dictionary<CharacterStat, int>();
            foreach (var stat in mBaseStats)
            {
                total.Add(stat.Key, stat.Value + GetTotalStatModifier(stat.Key));
            }

            return total;
        }
    }
    public int CurHp;
    public string CharacterName { get; protected set; }
    public int Level { get; protected set; }

    public CombatController Controller
    {
        get
        {
            return BaseCharacter.Controller;
        }
    }

    public Character(CharacterStats characterStats, string name="")
    {
        if (name == "")
        {
            CharacterName = characterStats.CharacterName;
        }
        else
        {
            CharacterName = name;
        }

        BaseCharacter = characterStats;
        CurHp = BaseCharacter.MaxHp;

        Level = characterStats.Level;

        mCharacterStatModifier = new Dictionary<CharacterStat, List<StatModifier>>
        {
            { CharacterStat.Attack, new List<StatModifier>() },
            { CharacterStat.Defense, new List<StatModifier>() },
            { CharacterStat.Magic, new List<StatModifier>() },
            { CharacterStat.Speed, new List<StatModifier>() },
            { CharacterStat.Hp, new List<StatModifier>() }
        };

        mBaseStats = new Dictionary<CharacterStat, int>
        {
            { CharacterStat.Attack, BaseCharacter.Attack },
            { CharacterStat.Defense, BaseCharacter.Defense },
            { CharacterStat.Magic, BaseCharacter.Magic },
            { CharacterStat.Speed, BaseCharacter.Speed },
            { CharacterStat.Hp, BaseCharacter.MaxHp }
        };
    }

    public void TurnPass()
    {
        DurationPassed(StatModifier.ModifierDurationType.Turns);
    }

    public void AddStatModifier(CharacterStat stat, StatModifier statModifier)
    {
        if (statModifier.Modifier != 0 && (statModifier.Duration > 0 || statModifier.DurationType == StatModifier.ModifierDurationType.Permanent))
        {
            mCharacterStatModifier[stat].Add(statModifier);
        }
    }

    protected void CleanupModifiers(CharacterStat stat)
    {
        mCharacterStatModifier[stat].RemoveAll((mod) => { return mod.Duration <= 0; });
    }

    protected void DurationPassed(StatModifier.ModifierDurationType duration)
    {
        foreach (var statModifer in mCharacterStatModifier)
        {
            statModifer.Value.ForEach((mod) => {
                if (mod.DurationType == duration)
                {
                    mod.Duration--;
                }
            });

            CleanupModifiers(statModifer.Key);
        }
    }

    protected virtual int GetTotalStatModifier(CharacterStat stat)
    {
        List<StatModifier> mod = mCharacterStatModifier[stat];

        int total = mod.Sum((m) => { return m.Modifier; });
        return total;
    }

    public virtual BaseMagic OffensiveSpell()
    {
        return null;
    }

    public virtual BaseDefenseMagic DefenseSpell()
    {
        return null;
    }

    public virtual void CombatSkill()
    {

    }
}
