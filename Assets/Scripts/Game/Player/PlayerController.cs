﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private static Camera MainCamera;

    public GameObject PlayerCameraObject;

    public delegate void PlayerMovedDelegate(PathfindingGrid.Direction direction);
    public delegate void PlayerArrivedDelegate(RoutingNode endNode, bool forceEnd=false);

    public PlayerMovedDelegate OnPlayerMoved;
    public PlayerArrivedDelegate OnPlayerArrived;

    public bool AbleToMove = false;
    public PathfindingGrid.Direction LastDirectionMoved;

    RoutingAgent mAgent;
    RoutingNode mNode;
    PathfindingGrid mPathfindingGrid;

    bool bIsPathing = false;
    bool bIsPlayerTurn = false;

    Transform mPlayerModel;

    public void OnTurnStart()
    {
        if (MainCamera == null)
            MainCamera = Camera.main;

        MainCamera.transform.position = PlayerCameraObject.transform.position;
        bIsPlayerTurn = true;
    }

    public void OnTurnEnd()
    {
        bIsPlayerTurn = false;

        if (mPlayerModel != null)
            mPlayerModel.localRotation = Quaternion.Euler(0, 180, 0);
    }

    private void Start()
    {        
        mAgent = GetComponent<RoutingAgent>();
        mAgent.OnPathingComplete += PathingComplete;

        mPathfindingGrid = PathfindingGrid.instance;

        mPlayerModel = transform.Find("CharacterModel");
    }

    public RoutingNode GetCurrentNode()
    {
        if (mNode == null)
            mNode = GetClosestNode();

        return mNode;
    }

    private RoutingNode GetClosestNode()
    {
        RoutingNode[] nodes = Routing.Instance.RoutingNodes;

        float dist = float.MaxValue;
        RoutingNode closestNode = null;

        foreach(RoutingNode node in nodes)
        {
            float newDist = Vector3.Distance(transform.position, node.transform.position);
            if (newDist < dist)
            {
                dist = newDist;
                closestNode = node;
            }
        }

        return closestNode;
    }

    void PathingComplete()
    {
        bIsPathing = false;

        OnPlayerArrived?.Invoke(mNode);
    }

    public void ForceMove(PathfindingGrid.Direction forcedDirection)
    {
        mPlayerModel.localRotation = Quaternion.Euler(0, 90 * (int)forcedDirection, 0);

        mNode = mPathfindingGrid.GetDirectedNeighbour(mNode.transform.position, forcedDirection);
        mAgent.TargetRoutingNode = mNode;        
        bIsPathing = true;
    }

    private void Update()
    {
        if (bIsPlayerTurn)
        {
            MainCamera.transform.position = PlayerCameraObject.transform.position;
        }

        if (bIsPathing || !AbleToMove)
        {
            return;
        }

        RoutingNode node = null;
        if (mNode.CanMoveLeft && Input.GetKey(KeyCode.LeftArrow))
        {
            LastDirectionMoved = PathfindingGrid.Direction.Left;
            node = mPathfindingGrid.GetDirectedNeighbour(mNode.transform.position, LastDirectionMoved);

            mPlayerModel.localRotation = Quaternion.Euler(0, 270, 0);
        }
        else if (mNode.CanMoveRight && Input.GetKey(KeyCode.RightArrow))
        {
            LastDirectionMoved = PathfindingGrid.Direction.Right;
            node = mPathfindingGrid.GetDirectedNeighbour(mNode.transform.position, LastDirectionMoved);

            mPlayerModel.localRotation = Quaternion.Euler(0, 90, 0);
        }            
        else if (mNode.CanMoveUp && Input.GetKey(KeyCode.UpArrow))
        {
            LastDirectionMoved = PathfindingGrid.Direction.Up;
            node = mPathfindingGrid.GetDirectedNeighbour(mNode.transform.position, LastDirectionMoved);

            mPlayerModel.localRotation = Quaternion.Euler(0, 0, 0);
        }            
        else if (mNode.CanMoveDown && Input.GetKey(KeyCode.DownArrow))
        {
            LastDirectionMoved = PathfindingGrid.Direction.Down;
            node = mPathfindingGrid.GetDirectedNeighbour(mNode.transform.position, LastDirectionMoved);

            mPlayerModel.localRotation = Quaternion.Euler(0, 180, 0);
        }            

        if (node != null)
        {
            mAgent.TargetRoutingNode = node;
            mNode = node;
            bIsPathing = true;

            OnPlayerMoved?.Invoke(LastDirectionMoved);
        }
    }
}
