﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class PlayerCustomizationController : MonoBehaviour
{
    public SkinnedMeshRenderer FaceMesh;
    public SkinnedMeshRenderer BodyMesh;
    public SkinnedMeshRenderer[] ColouredItems;

    public GameObject PlayerModel;
    public GameObject CoffinModel;

    public void Die()
    {
        CoffinModel.SetActive(true);
        PlayerModel.SetActive(false);
    }

    public void Live()
    {
        CoffinModel.SetActive(false);
        PlayerModel.SetActive(true);
    }

    public void SwapFaces(Material newFace)
    {

    }

    public void SwapMaterials(Material newMaterial)
    {
        foreach (SkinnedMeshRenderer smr in ColouredItems)
        {
            smr.material = newMaterial;
        }
    }
}
