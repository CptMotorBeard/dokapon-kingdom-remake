﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Job", menuName = "Game/New Starting Job")]
public class StartingPlayerJob : PlayerJob
{
    public CharacterStats BaseStats;
}