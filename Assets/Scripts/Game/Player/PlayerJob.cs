﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelUpInfo
{
    public CharacterStat StatToIncrease;
    public int IncreaseAmount;
}

public enum MasteryRequirementType
{
    Any,
    All
}

[CreateAssetMenu(fileName = "New Job", menuName = "Game/New Job")]
public class PlayerJob : ScriptableObject
{
    public string JobName;
    public CharacterStat MasteryStat;

    public float PayRaiseMultiplier;
    public int BaseSalary;
    // SalaryBonus

    public int ItemInventorySize;
    public int MagicInventorySize;

    public MasteryRequirementType JobMasteryRequirement;
    public List<PlayerJob> JobMasteryRequirements;
    public List<LevelUpInfo> LevelUpStats;

    // FieldSkill
    // List<BattleSkills> LearnableSkills --> requires level learned + skill learned
}
