﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public class PlayerMapData
{
    public Color PlayerColor;
    public Vector3 Location;
}

public class Player : Character
{
    public PlayerMapData MapData;

    private long mTotalGold;
    private ulong mTotalExperience;
    public long TotalGold
    {
        get
        {
            return mTotalGold;
        }
        private set
        {
            mTotalGold = value;
            OnPlayerGoldChanged?.Invoke();
        }
    }
    public ulong TotalExperience
    {
        get
        {
            return mTotalExperience;
        }
        private set
        {
            mTotalExperience = value;
            OnPlayerExpChanged?.Invoke();
        }
    }
    private long mTotalTownValue;
    private long mTotalCastleValue;
    public long TotalValue => mTotalGold + mTotalTownValue + mTotalCastleValue;

    PlayerJob mCurrentJob;
    List<PlayerJob> mMasteredJobs;
    Dictionary<PlayerJob, int> mJobMasteryLevels;
    Dictionary<CharacterStat, int> mMasteryStats;
    Dictionary<CharacterStat, int> mTotalLevelUpBonuses;

    PlayerInventory mItemInventory;
    PlayerInventory mMagicInventory;
    PlayerEquipment mEquipmentInventory;

    int mForcedMovementSpaces = -1;
    int mNumberOfSpinners = 1;
    public int ForcedMovementSpaces
    {
        get
        {
            return mForcedMovementSpaces;
        }
        set
        {
            mForcedMovementSpaces = value;
            OnPlayerMovementSet?.Invoke();
            mForcedMovementSpaces = -1;
        }
    }
    public int NumberOfSpinners
    {
        get
        {
            return mNumberOfSpinners;
        }
        set
        {
            mNumberOfSpinners = value;
            OnPlayerSpinnerSet?.Invoke();
            mNumberOfSpinners = 1;
        }
    }

    public bool IsDead = false;
    public int DeathTimer = 0;

    public PlayerJob Job => mCurrentJob;
    public IReadOnlyDictionary<PlayerJob, int> JobMasteryLevels => mJobMasteryLevels;
    public int CurrentJobMasteryLevel
    {
        get
        {
            if (mJobMasteryLevels.ContainsKey(mCurrentJob))
            {
                return mJobMasteryLevels[mCurrentJob];
            }

            return 1;
        }
    }

    public IReadOnlyList<Item> ItemInventory => mItemInventory.Items;
    public IReadOnlyList<Item> MagicInventory => mMagicInventory.Items;
    public IReadOnlyDictionary<ItemType, Item> Equipment => mEquipmentInventory.Equipment;

    #region Callbacks
    public delegate void PlayerGoldChangedDelegate();
    public delegate void PlayerExperienceChangedDelegate();
    public delegate void PlayerMovementSetDelegate();
    public delegate void PlayerSpinnerSetDelegate();
    public delegate void PlayerLevelUpDelegate(int timesLeveled);
    public PlayerGoldChangedDelegate OnPlayerGoldChanged;
    public PlayerExperienceChangedDelegate OnPlayerExpChanged;
    public PlayerMovementSetDelegate OnPlayerMovementSet;
    public PlayerSpinnerSetDelegate OnPlayerSpinnerSet;
    public PlayerLevelUpDelegate OnPlayerLevelUp;
    #endregion

    public Player(StartingPlayerJob startingJob, string name) : base(startingJob.BaseStats, name)
    {
        mCurrentJob = startingJob;

        TotalGold = 0;
        mTotalTownValue = 0;
        mTotalCastleValue = 0;

        mItemInventory = new PlayerInventory(mCurrentJob.ItemInventorySize);
        mMagicInventory = new PlayerInventory(mCurrentJob.MagicInventorySize);
        mEquipmentInventory = new PlayerEquipment();

        mMasteredJobs = new List<PlayerJob>();
        mMasteryStats = new Dictionary<CharacterStat, int>();
        mTotalLevelUpBonuses = new Dictionary<CharacterStat, int>();
        mJobMasteryLevels = new Dictionary<PlayerJob, int>();
        mJobMasteryLevels.Add(mCurrentJob, 1);
    }

    public void Heal(int healAmount)
    {
        var maxHp = CharacterStats[CharacterStat.Hp];
        CurHp = Mathf.Min(maxHp, CurHp + healAmount);
    }

    public void TakeDamage(int amount)
    {
        CurHp = Mathf.Max(0, CurHp - amount);        
    }

    public void CombatPass()
    {
        DurationPassed(StatModifier.ModifierDurationType.Combats);
    }

    public void DayPass()
    {
        DurationPassed(StatModifier.ModifierDurationType.Days);
    }

    public void WeekPass()
    {
        PayoutSalary();
        DurationPassed(StatModifier.ModifierDurationType.Weeks);
    }

    public void Equip(Item item)
    {
        Item oldEquip = mEquipmentInventory.Equip(item);
        if (oldEquip != null)
        {
            Unequip(oldEquip);
        }

        Equipment e = item as Equipment;
        if (e == null)
        {
            return;
        }

        StatModifier s = new StatModifier(StatModifier.ModifierDurationType.Permanent, 0, e.Attack)
        {
            Id = e.Name
        };
        AddStatModifier(CharacterStat.Attack, s);

        s = new StatModifier(StatModifier.ModifierDurationType.Permanent, 0, e.Defense)
        {
            Id = e.Name
        };
        AddStatModifier(CharacterStat.Defense, s);

        s = new StatModifier(StatModifier.ModifierDurationType.Permanent, 0, e.Magic)
        {
            Id = e.Name
        };
        AddStatModifier(CharacterStat.Magic, s);

        s = new StatModifier(StatModifier.ModifierDurationType.Permanent, 0, e.Speed)
        {
            Id = e.Name
        };
        AddStatModifier(CharacterStat.Speed, s);

        s = new StatModifier(StatModifier.ModifierDurationType.Permanent, 0, e.Hp)
        {
            Id = e.Name
        };
        AddStatModifier(CharacterStat.Hp, s);
    }

    public void Unequip(Item item, bool removeFromInventory=false)
    {
        if (removeFromInventory)
        {
            mEquipmentInventory.Unequip(item.Type);
        }

        string id = item.Name;

        foreach (var statModifier in mCharacterStatModifier)
        {
            StatModifier s = statModifier.Value.Find((mod) => { return mod.Id == id; });
            statModifier.Value.Remove(s);
        }
    }

    public override BaseMagic OffensiveSpell()
    {
        return Equipment[ItemType.OffMagic]?.GetAsOffMagic();
    }

    public override BaseDefenseMagic DefenseSpell()
    {
        return Equipment[ItemType.DefMagic]?.GetAsDefMagic();
    }

    protected override int GetTotalStatModifier(CharacterStat stat)
    {
        int s = base.GetTotalStatModifier(stat);
        if (mTotalLevelUpBonuses.ContainsKey(stat))
        {
            if (stat == CharacterStat.Hp)
            {
                s += 10 * mTotalLevelUpBonuses[stat];
            }
            else
            {
                s += mTotalLevelUpBonuses[stat];
            }            
        }

        return s;
    }

    public void LevelUp(Dictionary<CharacterStat, int> selectedBonus, int timesLeveled)
    {
        var totalBonus = GetLevelUpBonus();
        foreach (var stat in selectedBonus.Keys)
        {
            if (mTotalLevelUpBonuses.ContainsKey(stat))
            {
                mTotalLevelUpBonuses[stat] += selectedBonus[stat];
            }
            else
            {
                mTotalLevelUpBonuses.Add(stat, selectedBonus[stat]);
            }
        }

        foreach (var stat in totalBonus.Keys)
        {
            if (mTotalLevelUpBonuses.ContainsKey(stat))
            {
                mTotalLevelUpBonuses[stat] += (totalBonus[stat] * timesLeveled);
            }
            else
            {
                mTotalLevelUpBonuses.Add(stat, (totalBonus[stat] * timesLeveled));
            }
        }

        Level += timesLeveled;
        CurHp = CharacterStats[CharacterStat.Hp];
    }

    public Dictionary<CharacterStat, int> GetLevelUpBonus()
    {
        Dictionary<CharacterStat, int> bonus = new Dictionary<CharacterStat, int>();
        foreach (var stat in mCurrentJob.LevelUpStats)
        {
            if (bonus.ContainsKey(stat.StatToIncrease))
            {
                bonus[stat.StatToIncrease] += stat.IncreaseAmount;
            }
            else
            {
                bonus.Add(stat.StatToIncrease, stat.IncreaseAmount);
            }
        }

        foreach (var stat in mMasteryStats)
        {
            if (bonus.ContainsKey(stat.Key))
            {
                bonus[stat.Key] += stat.Value;
            }
            else
            {
                bonus.Add(stat.Key, stat.Value);
            }
        }

        return bonus;
    }

    public void AddGold(long amount)
    {
        TotalGold += amount;
    }

    public void AddExperience(ulong amount)
    {
        TotalExperience += amount;

        int newLevel = Level;
        while (TotalExperience > PlayerUtils.GetRequiredXpByLevel(newLevel + 1))
        {
            ++newLevel;
        }

        if (newLevel > Level)
        {
            int levelDiff = newLevel - Level;
            OnPlayerLevelUp?.Invoke(levelDiff);
        }
    }

    public void ClearInventory(ItemType t)
    {
        if (t == ItemType.Item)
        {
            mItemInventory.Clear();
        }
        else if (t == ItemType.FieldMagic)
        {
            mMagicInventory.Clear();
        }
        else
        {
            Debug.LogError("Invalid inventory type");
        }
    }

    public List<Item> AddItems(List<Item> items)
    {
        List<Item> returnItems = new List<Item>();
        foreach (Item item in items)
        {
            Item i = AddItem(item);
            if (i)
                returnItems.Add(i);
        }

        return returnItems;
    }

    public Item AddItem(Item item)
    {
        if (item == null)
        {
            Debug.LogError("Adding null item to inventory");
            return null;
        }

        if (item.Type == ItemType.Item)    
            return mItemInventory.AddItem(item);
        else if (item.Type == ItemType.FieldMagic)
            return mMagicInventory.AddItem(item);
        else if (item.IsEquipment)
        {
            Equip(item);
            return null;
        }

        Debug.LogError("Trying to add a non-item to the inventory");
        return null;
    }

    public bool RemoveItem(Item item)
    {
        if (item == null)
        {
            Debug.LogError("Removing null item from inventory");
            return false;
        }

        if (item.Type == ItemType.Item)
            return mItemInventory.RemoveItem(item);
        else if (item.Type == ItemType.FieldMagic)
            return mMagicInventory.RemoveItem(item);

        Debug.LogError("Trying to remove a non-item from the inventory");
        return false;
    }

    public bool CanPurchaseItem(Item item)
    {
        if (TotalGold >= item.Price)
        {
            return true;
        }

        return false;
    }

    public Item PurchaseItem(Item item)
    {
        if (!CanPurchaseItem(item))
        {
            return null;
        }

        TotalGold -= item.Price;
        return AddItem(item);
    }

    public bool TrySell(Item item, float sellPercent)
    {
        long value = Mathf.FloorToInt(item.Price * sellPercent);

        if (item.Type == ItemType.Item && mItemInventory.RemoveItem(item) ||
           (item.Type == ItemType.FieldMagic && mMagicInventory.RemoveItem(item)))
        {
            AddGold(value);
            return true;
        }
        else if (item.IsEquipment)
        {
            Unequip(item, true);
            AddGold(value);
            return true;
        }

        Debug.LogError("Trying to sell an invalid item");
        return false;
    }

    public void ForceSell(Item item, float sellPercent)
    {
        long value = Mathf.FloorToInt(item.Price * sellPercent);
        AddGold(value);
    }

    private void PayoutSalary()
    {
        foreach (var job in mJobMasteryLevels)
        {
            int jobSalary = job.Key.BaseSalary;

            for (int i = 1; i < job.Value; i++)
            {
                jobSalary = Mathf.CeilToInt(jobSalary * job.Key.PayRaiseMultiplier);
            }

            TotalGold += jobSalary;
        }
    }
}