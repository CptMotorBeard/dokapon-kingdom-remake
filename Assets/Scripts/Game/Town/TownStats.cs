﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
 
[CreateAssetMenu(fileName = "New Town", menuName = "Game/Town/New Town")]
public class TownStats : ScriptableObject
{
    public string TownName;
    public ulong InitialTownWorth;
    public uint InitalTaxes;

    public EnemyStats[] PossibleBossMonsters;
}
