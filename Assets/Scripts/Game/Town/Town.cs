﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
public class Town
{
    public TownStats Stats;
    public int TownLevel;
    public Player Owner;
    public Character BossMonster;

    public Town(TownStats town)
    {
        Stats = town;
        TownLevel = 0;
        Owner = null;
    }
}
