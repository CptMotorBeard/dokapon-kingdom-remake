﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class TownManager : MonoBehaviour
{
    public static TownManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public TownUiManager TownUi;

    public void Initialize(Town town)
    {
        TownUi.Initialize(town);
    }

    public void SleepToExit() //-V3013
    {
        // Heal player
        // Spend money

        GlobalGameManager.Instance.SwitchToBoardScene();
    }

    public void DoNothingAndExit()
    {
        GlobalGameManager.Instance.SwitchToBoardScene();
    }
}
