﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

public abstract class CombatController
{
    public CombatManager.Action CurrentAction { get; private set; }

    protected CombatMath.AbilityOptions mSkillUsed;

    public abstract void OnUpKey();
    public abstract void OnDownKey();
    public abstract void OnLeftKey();
    public abstract void OnRightKey();

    public virtual void TurnStart(CombatManager.Action actionTaking)
    {
        CurrentAction = actionTaking;
        mSkillUsed = CombatMath.AbilityOptions.NotReady;
    }

    public virtual void Initialize(CombatManager.Action actionTaking)
    {
        TurnStart(actionTaking);
    }

    public CombatMath.AbilityOptions GetSkillUsed()
    {
        return mSkillUsed;
    }
}
