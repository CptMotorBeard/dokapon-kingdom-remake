﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DropListItem
{
    public Item DropItem;
    public int Weight;
}

[CreateAssetMenu(fileName ="New Enemy", menuName = "Game/New Enemy Stats")]
public class EnemyStats : CharacterStats
{
    public uint XPGained;
    public ulong GoldGained;
    [Range(0, 100)]
    public int DropChance;
    public List<DropListItem> PossibleDrops;

    public Item GetDrop()
    {
        Item item = null;

        if (Random.Range(0, 100) < DropChance)
        {
            int totalWeight = PossibleDrops.Sum((i) => { return i.Weight; });
            int r = Random.Range(1, totalWeight + 1);

            int currentWeight = 0;
            foreach (DropListItem dropListItem in PossibleDrops)
            {
                currentWeight += dropListItem.Weight;
                if (currentWeight >= r)
                {
                    item = dropListItem.DropItem;
                    break;
                }
            }
        }        

        return item;
    }
}
