﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class CombatCharacter : MonoBehaviour
{
    public CombatPlayerUiManager CharacterUiManager;

    public Character CharacterBackend { get; private set; }
    
    bool mReadyForAction = false;
    CombatController mController;

    public int MaxHp
    {
        get
        {
            if (CharacterBackend == null)
                return 9999;
            return CharacterBackend.CharacterStats[CharacterStat.Hp];
        }
    }

    public int CurHp
    {
        get
        {
            if (CharacterBackend == null)
                return 9999;
            return CharacterBackend.CurHp;
        }
    }

    public int Attack
    {
        get
        {
            if (CharacterBackend == null)
                return 999;
            return CharacterBackend.CharacterStats[CharacterStat.Attack];
        }
    }

    public int Defense
    {
        get
        {
            if (CharacterBackend == null)
                return 999;
            return CharacterBackend.CharacterStats[CharacterStat.Defense];
        }
    }

    public int Magic
    {
        get
        {
            if (CharacterBackend == null)
                return 999;
            return CharacterBackend.CharacterStats[CharacterStat.Magic];
        }
    }

    public int Speed
    {
        get
        {
            if (CharacterBackend == null)
                return 999;
            return CharacterBackend.CharacterStats[CharacterStat.Speed];
        }
    }

    public CharacterStats BaseCharacterStats
    {
        get
        {
            return CharacterBackend.BaseCharacter;
        }
    }

    public void TakeDamage(float amount)
    {
        CharacterBackend.CurHp = (int)(CharacterBackend.CurHp - amount);
        CharacterBackend.CurHp = Mathf.Max(0, CharacterBackend.CurHp);

        Debug.Log($"{CharacterBackend.CharacterName} took {amount} damage, left with {CharacterBackend.CurHp} health");
        CharacterUiManager.HealthBarsAnimating = true;
    }

    public void SetCharacterStats(Character character, CombatManager.Action actionTaking)
    {
        if (character == null)
        {
            Debug.LogError("Trying to initialize a combat character with a null character");
            return;
        }

        CharacterBackend = character;

        mController = character.Controller;
        mController.Initialize(actionTaking);

        CharacterUiManager.InitializeUi(this, actionTaking);
    }

    public void Update()
    {
        if (!mReadyForAction)
            return;

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            mController.OnUpKey();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            mController.OnDownKey();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            mController.OnLeftKey();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            mController.OnRightKey();
        }
    }

    public float GetOffensiveMagicPower()
    {
        return CharacterBackend.OffensiveSpell().Power;
    }

    public int GetDefensiveMagicPower()
    {
        if (mController.GetSkillUsed() == CombatMath.AbilityOptions.MagicDefend)
            return CharacterBackend.DefenseSpell().Power;
        
        return 0;
    }

    public void TurnStart(CombatManager.Action newAction)
    {
        mController.TurnStart(newAction);
        CharacterUiManager.TurnStart(newAction);
        mReadyForAction = false;
    }

    public CombatMath.AbilityOptions GetSkillUsed()
    {
        if (!mReadyForAction)
        {
            mReadyForAction = true;
            return CombatMath.AbilityOptions.NotReady;
        }            
        return mController.GetSkillUsed();
    }

    public void PerformAction(CombatCharacter opponent)
    {
        Debug.Log($"{mController.CurrentAction}: { CharacterBackend.CharacterName } performed action {mController.GetSkillUsed()}");

        CombatMath.AbilityOptions guard = opponent.GetSkillUsed();
        CombatMath.AbilityOptions attack = GetSkillUsed();

        if (guard == CombatMath.AbilityOptions.Counter && attack == CombatMath.AbilityOptions.Strike)
        {
            TakeDamage(CombatMath.GetCounterDamage(Attack, Defense, opponent.Attack, opponent.Magic, opponent.Speed));
        }
        else
        {
            switch(attack)
            {
                case CombatMath.AbilityOptions.Attack:
                    opponent.TakeDamage(CombatMath.GetPhysicalDamage(Attack, opponent.Defense, CombatMath.GetPhysicalGuardValue(guard)));
                    break;
                case CombatMath.AbilityOptions.Magic:                    
                    float damage = CombatMath.GetMagicalDamage(Magic, opponent.Magic, GetOffensiveMagicPower(), opponent.GetDefensiveMagicPower(), CombatMath.GetMagicalGuardValue(guard));
                    opponent.TakeDamage(damage);
                    CharacterBackend.OffensiveSpell().OnCast(this, opponent);

                    if (guard == CombatMath.AbilityOptions.MagicDefend && opponent.CurHp > 0)
                    {
                        opponent.CharacterBackend.DefenseSpell().OnCast(opponent, this);
                    }
                    break;
                case CombatMath.AbilityOptions.Strike:
                    opponent.TakeDamage(CombatMath.GetStrikeDamage(Attack, Magic, Speed, opponent.Defense, opponent.Magic, opponent.Speed, CombatMath.GetStrikeGuardValue(guard)));
                    break;
            }
        }
    }
}
