﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
public class CombatTurnManager
{
    public CombatCharacter Attacker { get; private set; }
    public CombatCharacter Defender { get; private set; }
    public int TurnsElapsed { get; private set; }

    CombatMath.AbilityOptions mAttackSkillUsed;
    CombatMath.AbilityOptions mDefenseSkillUsed;

    bool bQueuedEndTurn = false;

    public CombatTurnManager(CombatCharacter attacker, CombatCharacter defender)
    {
        Attacker = attacker;
        Defender = defender;

        Attacker.TurnStart(CombatManager.Action.AttackAction);
        Defender.TurnStart(CombatManager.Action.DefenseAction);
    }

    public void NextTurn()
    {
        mAttackSkillUsed = CombatMath.AbilityOptions.NotReady;
        mDefenseSkillUsed = CombatMath.AbilityOptions.NotReady;

        if (Attacker.CharacterUiManager.HealthBarsAnimating || Defender.CharacterUiManager.HealthBarsAnimating)
        {
            bQueuedEndTurn = true;
            return;
        }

        ++TurnsElapsed;
        CombatCharacter t = Attacker;
        Attacker = Defender;
        Defender = t;

        Attacker.TurnStart(CombatManager.Action.AttackAction);
        Defender.TurnStart(CombatManager.Action.DefenseAction);
    }

    public void Tick()
    {
        if (Attacker.CurHp <= 0 || Defender.CurHp <= 0)
        {
            return;
        }

        if (Attacker.CharacterUiManager.HealthBarsAnimating || Defender.CharacterUiManager.HealthBarsAnimating)
        {
            return;
        }

        if (bQueuedEndTurn)
        {
            bQueuedEndTurn = false;
            NextTurn();

            return;
        }

        if (mAttackSkillUsed == CombatMath.AbilityOptions.NotReady)
            mAttackSkillUsed = Attacker.GetSkillUsed();
        else if (mDefenseSkillUsed == CombatMath.AbilityOptions.NotReady)
            mDefenseSkillUsed = Defender.GetSkillUsed();
    }

    public bool PlayerActionsComplete()
    {
        if ((mAttackSkillUsed == CombatMath.AbilityOptions.NotReady) || (mDefenseSkillUsed == CombatMath.AbilityOptions.NotReady))
        {
            return false;
        }

        return true;
    }
}
