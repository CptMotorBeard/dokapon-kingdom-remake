﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Game/New Character Stats")]
public class CharacterStats : ScriptableObject
{
    public enum AiPersonality
    {
        Default,
        Player,
        CannotBeStrike
    }

    public string CharacterName;

    public int Level;

    public int MaxHp;

    public int Attack;
    public int Defense;
    public int Magic;
    public int Speed;

    public AiPersonality Personality;

    CombatController mController;
    public CombatController Controller
    {
        get
        {
            if (mController == null)
            {
                switch (Personality)
                {
                    case AiPersonality.Player:
                        mController = new CombatPlayerController();
                        break;
                    case AiPersonality.Default:
                    case AiPersonality.CannotBeStrike:
                    default:
                        mController = new CombatAiController();
                        break;
                }
            }

            return mController;
        }
    }
}
