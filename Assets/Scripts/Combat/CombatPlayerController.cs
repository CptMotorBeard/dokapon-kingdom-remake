﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

public class CombatPlayerController : CombatController
{
    public override void TurnStart(CombatManager.Action actionTaking)
    {
        mSkillUsed = CombatMath.AbilityOptions.NotReady;

        base.TurnStart(actionTaking);
    }

    public override void OnDownKey()
    {
        if (mSkillUsed == CombatMath.AbilityOptions.NotReady)
        {
            if (CurrentAction == CombatManager.Action.AttackAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.Skill;
            }
            else if (CurrentAction == CombatManager.Action.DefenseAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.GiveUp;
            }
        }        
    }

    public override void OnLeftKey()
    {
        if (mSkillUsed == CombatMath.AbilityOptions.NotReady)
        {
            if (CurrentAction == CombatManager.Action.AttackAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.Strike;
            }
            else if (CurrentAction == CombatManager.Action.DefenseAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.Counter;
            }
        }        
    }

    public override void OnRightKey()
    {
        if (mSkillUsed == CombatMath.AbilityOptions.NotReady)
        {
            if (CurrentAction == CombatManager.Action.AttackAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.Attack;
            }
            else if (CurrentAction == CombatManager.Action.DefenseAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.Defend;
            }
        }        
    }

    public override void OnUpKey()
    {
        if (mSkillUsed == CombatMath.AbilityOptions.NotReady)
        {
            if (CurrentAction == CombatManager.Action.AttackAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.Magic;
            }
            else if (CurrentAction == CombatManager.Action.DefenseAction)
            {
                mSkillUsed = CombatMath.AbilityOptions.MagicDefend;
            }
        }        
    }
}
