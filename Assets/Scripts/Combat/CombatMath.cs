﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

// Source https://dokapon.fandom.com/wiki/Damage
public class CombatMath
{
    public enum AbilityOptions
    {
        NotReady,
        Defend,
        MagicDefend,
        Counter,
        None,
        GiveUp,
        Attack,
        Strike,
        Magic,
        Skill    
    }

    public static int GetPhysicalGuardValue(AbilityOptions guard)
    {
        switch (guard)
        {
            case AbilityOptions.Defend:
                return 50;
            case AbilityOptions.MagicDefend:
                return 70;
            case AbilityOptions.Counter:
                return 90;
            case AbilityOptions.None:
            default:
                return 100;
        }
    }

    public static int GetMagicalGuardValue(AbilityOptions guard)
    {
        switch (guard)
        {
            case AbilityOptions.Defend:
                return 70;
            case AbilityOptions.MagicDefend:
                return 50;
            case AbilityOptions.Counter:
                return 90;
            case AbilityOptions.None:
            default:
                return 100;
        }
    }

    public static int GetStrikeGuardValue(AbilityOptions guard)
    {
        switch (guard)
        {
            case AbilityOptions.Defend:
                return 64;
            case AbilityOptions.MagicDefend:
                return 68;
            case AbilityOptions.Counter:
                return 0;
            case AbilityOptions.None:
            default:
                return 100;
        }
    }

    // Variance is a number that is exactly 95% or 105%
    public static float GetVariance()
    {
        int flip = Random.Range(0, 2);
        int variance = 95 + (flip * 10);

        return variance / 100f;
    }

    // Damage = (Atk.AT * 5.6 - Dfn.DF * 2.4) * Guard * Variance    
    public static float GetPhysicalDamage(int attack, int defense, int guard)
    {        
        float modifier = (guard / 100f) * GetVariance();
        float damage = (attack * 5.6f - defense * 2.4f) * modifier;

        return Mathf.Max(0, damage);
    }

    // Damage = (Atk.MG* 2.4 - Dfn.MG) * Offensive Power * (1 - Defensive Power) * Guard * Variance
    public static float GetMagicalDamage(int attackerMagic, int defenderMagic, float offensivePower, int defensivePower, int guard)
    {
        float modifier = offensivePower * (1 - (defensivePower / 100f)) * (guard / 100f) * GetVariance();
        float damage = (attackerMagic * 2.4f - defenderMagic) * modifier;

        return Mathf.Max(0, damage);
    }

    // Damage = ((Atk.AT + Atk.MG + Atk.SP) * 6.25 - (Dfn.DF + Dfn.MG + Dfn.SP) * 2.5) * Guard * Variance
    public static float GetStrikeDamage(int attackerAttack, int attackerMagic, int attackerSpeed, int defenderAttack, int defenderMagic, int defenderSpeed, int guard)
    {
        float attackPower = (attackerAttack + attackerMagic + attackerSpeed) * 6.25f;
        float defensePower = (defenderAttack + defenderMagic + defenderSpeed) * 2.5f;
        float modifier = (guard / 100f) * GetVariance();
        float damage = (attackPower - defensePower) * modifier;

        return Mathf.Max(0, damage);
    }

    // Damage = ((Dfn.AT + Dfn.MG + Dfn.SP) * 4 + (Atk.AT - Atk.DF) * 2) * Variance
    public static float GetCounterDamage(int attackerAttack, int attackerDefense, int defenderAttack, int defenderMagic, int defenderSpeed)
    {
        float defenderAttackPower = (defenderAttack + defenderMagic + defenderSpeed) * 4.0f;
        float attackerAttackPower = (attackerAttack - attackerDefense) * 2.0f;
        float damage = (defenderAttackPower + attackerAttackPower) * GetVariance();

        return Mathf.Max(0, damage);
    }

    // Damage = (Atk.MG* 2 - Dfn.MG) * Spell Power * Variance
    // Unlike in Battle Damage, this variance is a random number between 100% and 110%
    // minimum of 1 damage
    public static float GetOverworldMagicDamage(int attackerMagic, int defenderMagic, float spellPower)
    {
        int variance = Random.Range(100, 111);
        float modifier = spellPower * (variance / 100f);
        float damage = (attackerMagic * 2 - defenderMagic) * modifier;

        return Mathf.Max(1, damage);
    }
}
