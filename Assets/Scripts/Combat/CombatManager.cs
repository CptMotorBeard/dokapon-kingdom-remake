﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public static CombatManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {           
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public CharacterStats DEBUG_Enemy;
    public CharacterStats DEBUG_Player;

    [HideInInspector()]
    public bool DEBUG_MODE = false;

    private void Start()
    {
        if (DEBUG_MODE)
        {
            Initialize(new Character(DEBUG_Player), new Character(DEBUG_Enemy));
            MAX_TURNS = 1000000;
        }            
    }

    public enum Action
    {
        AttackAction,
        DefenseAction
    }

    int MAX_TURNS = 1;

    public CombatUiManager UiManager;

    public CombatCharacter LeftSideCharacter;
    public CombatCharacter RightSideCharacter;

    public delegate void CombatEndDelegate(Character enemy, bool pvp);
    public CombatEndDelegate OnCombatEnd;

    CombatTurnManager mTurnManager;
    GlobalGameManager mGlobalGameManager;

    bool bIsRunning = false;
    bool bCombatInitialized = false;
    bool bIsPvp = false;

    public void Initialize(Character left, Character right, Material leftMaterial=null, Material rightMaterial=null, bool pvp=false)
    {
        mGlobalGameManager = GlobalGameManager.Instance;

        LeftSideCharacter.SetCharacterStats(left, Action.AttackAction);
        RightSideCharacter.SetCharacterStats(right, Action.DefenseAction);

        if (leftMaterial != null)
        {
            LeftSideCharacter.gameObject.GetComponent<PlayerCustomizationController>().SwapMaterials(leftMaterial);
        }

        if (rightMaterial != null)
        {
            RightSideCharacter.gameObject.GetComponent<MeshRenderer>().material = rightMaterial;
        }

        bIsRunning = true;
        bIsPvp = pvp;

        UiManager.Initialize();
    }

    private void Update()
    {
        if (!bIsRunning)
            return;

        if (UiManager.SwappingCards)
            return;

        if (!bCombatInitialized)
        {
            bool leftFirst = UiManager.LeftCharacterFirst;
            if (leftFirst)
            {
                mTurnManager = new CombatTurnManager(LeftSideCharacter, RightSideCharacter);
            }
            else
            {
                mTurnManager = new CombatTurnManager(RightSideCharacter, LeftSideCharacter);
            }

            bCombatInitialized = true;
        }

        bool stillFighting = mTurnManager.Attacker.CurHp > 0 && mTurnManager.Defender.CurHp > 0;
        if (stillFighting && mTurnManager.TurnsElapsed <= MAX_TURNS)
        {
            mTurnManager.Tick();
            if (mTurnManager.PlayerActionsComplete())
            {
                mTurnManager.Attacker.PerformAction(mTurnManager.Defender);
                mTurnManager.NextTurn();
            }
        }
        else if (!(mTurnManager.Attacker.CharacterUiManager.HealthBarsAnimating || mTurnManager.Defender.CharacterUiManager.HealthBarsAnimating))
        {
            bIsRunning = false;
            EndCombat();
        }
    }

    private void EndCombat()
    {
        if (DEBUG_MODE)
        {
            return;
        }

        OnCombatEnd?.Invoke(RightSideCharacter.CharacterBackend, bIsPvp);

        bool battleOver = !(LeftSideCharacter.CurHp > 0 && RightSideCharacter.CurHp > 0);
        CombatUiManager.Instance.EndCombat(battleOver, LeftSideCharacter.CurHp > 0, RightSideCharacter);
    }
}
