﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

public class CombatAiController : CombatController
{
    public override void TurnStart(CombatManager.Action actionTaking)
    {
        base.TurnStart(actionTaking);

        if (actionTaking == CombatManager.Action.AttackAction)
        {
            mSkillUsed = CombatMath.AbilityOptions.Attack;
        }
        else
        {
            mSkillUsed = CombatMath.AbilityOptions.Defend;
        }
    }

    public override void OnDownKey()
    {
        // Not required for AI
        return;
    }

    public override void OnLeftKey()
    {
        // Not required for AI
        return;
    }

    public override void OnRightKey()
    {
        // Not required for AI
        return;
    }

    public override void OnUpKey()
    {
        // Not required for AI
        return;
    }
}
