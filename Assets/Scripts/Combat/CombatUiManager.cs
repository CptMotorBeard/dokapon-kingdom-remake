﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CombatUiManager : MonoBehaviour
{
    public static CombatUiManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    [Header("Backend")]
    public HealthbarColor FullhealthColor;
    public HealthbarColor MissingHealthColor;
    public HealthbarColor LowHealthColor;

    public CombatPlayerUiManager LeftUi;
    public CombatPlayerUiManager RightUi;

    public GameObject CombatUiObject;
    public GameObject AttacksPanel;
    public GameObject VictoryObject;

    [Header("Cards")]
    public GameObject CardsUi;
    public Animator CardAnimator;
    public Image LeftReveal;
    public Image RightReveal;
    public Sprite FirstSprite;
    public Sprite LastSprite;

    [Header("UI Stuff")]
    public Sprite VictoryText;
    public Sprite DefeatText;
    public Image BattleEndStateImage;
    public GameObject RewardsPanel;

    [Header("Victory")]
    public TextMeshProUGUI GoldText;
    public TextMeshProUGUI XpText;

    [Header("Drop Item")]
    public GameObject DropItemObject;
    public TextMeshProUGUI DropItemText;

    [Header("Level Up")]
    public LevelUpController LevelUpPanel;

    EnemyStats mEnemy;
    GlobalGameManager mGlobalGameManager;

    private int mNumberOfTimesLeveledUp = 0;
    private bool bLeftFirst = true;
    private bool bSwappingCards = true;
    public bool SwappingCards => bSwappingCards;
    public bool LeftCharacterFirst => bLeftFirst;

    private enum CombatUiStates
    {
        Prep,
        SwappingCards,
        Battle,
        CombatResults,
        Defeated,
        PvpVictory,
        LootGain,
        LevelUp,
        Exit
    }

    private CombatUiStates mCurrentState;
    private CombatUiStates mNextState;

    private Dictionary<CombatUiStates, Action> mStateSwitchActions;    

    private void Start()
    {
        mGlobalGameManager = GlobalGameManager.Instance;
        mCurrentState = CombatUiStates.Prep;
        mNextState = CombatUiStates.SwappingCards;
        mGlobalGameManager.GetCurrentPlayer().OnPlayerLevelUp += PlayerLevelUp;

        mStateSwitchActions = new Dictionary<CombatUiStates, Action>()
        {
            // Non-action states aren't required here, but this keeps everything a bit more organized
            // { CombatUiStates.Prep, ()=>{ } },
            { CombatUiStates.SwappingCards, CardSwap },
            // { CombatUiStates.Battle, ()=>{ } },
            // { CombatUiStates.CombatResults, ()=>{ } },
            { CombatUiStates.Defeated, ()=>StartCoroutine(BattleEndStateAnimation(Defeated)) },
            { CombatUiStates.PvpVictory, ()=>StartCoroutine(BattleEndStateAnimation(DisplayPvpVictoryPanel)) },
            { CombatUiStates.LootGain, ()=>StartCoroutine(BattleEndStateAnimation(DisplayRewardsPanel)) },
            { CombatUiStates.LevelUp, DisplayLevelUpPanel },
            { CombatUiStates.Exit, ExitBattle }
        };
    }

    public void Initialize()
    {
        mNextState = CombatUiStates.SwappingCards;
        SwitchToNextState();
    }

    public void EndCombat(bool battleOver, bool won, CombatCharacter enemy)
    {
        mCurrentState = CombatUiStates.CombatResults;
        if (battleOver)
        {
            mEnemy = enemy.BaseCharacterStats as EnemyStats;

            VictoryObject.SetActive(true);
            CombatUiObject.SetActive(false);

            if (won)
            {
                BattleEndStateImage.sprite = VictoryText;

                if (mEnemy != null)
                {
                    mNextState = CombatUiStates.LootGain;
                }
                else
                {
                    mNextState = CombatUiStates.PvpVictory;
                }
            }
            else
            {
                mNextState = CombatUiStates.Defeated;
                BattleEndStateImage.sprite = DefeatText;
            }
        }
        else
        {
            mNextState = CombatUiStates.Exit;
        }

        SwitchToNextState();
    }

    public void SwitchToNextState()
    {
        mCurrentState = mNextState;
        if (mStateSwitchActions.ContainsKey(mCurrentState))
        {
            mStateSwitchActions[mCurrentState]();
        }
    }

    private void CardSwap()
    {
        bLeftFirst = UnityEngine.Random.Range(0, 2) == 0;
        mCurrentState = CombatUiStates.SwappingCards;
        mNextState = CombatUiStates.Battle;
    }

    private void Update()
    {
        if (mCurrentState == CombatUiStates.SwappingCards)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                CardAnimator.SetTrigger("SwapCards");
                bLeftFirst = !bLeftFirst;
            }

            if (Input.GetMouseButtonDown(0))
            {
                LeftReveal.gameObject.SetActive(true);
                RightReveal.gameObject.SetActive(true);
                LeftReveal.sprite = bLeftFirst ? FirstSprite : LastSprite;
                RightReveal.sprite = bLeftFirst ? LastSprite : FirstSprite;
                mCurrentState = CombatUiStates.Battle;
                mNextState = CombatUiStates.CombatResults;

                StartCoroutine(FinishSwappingCards());
            }
        }
    }

    private IEnumerator FinishSwappingCards()
    {
        yield return new WaitForSeconds(2);

        bSwappingCards = false;
        CardsUi.SetActive(false);
        AttacksPanel.SetActive(true);
    }

    private IEnumerator BattleEndStateAnimation(Action actionToTake)
    {
        yield return new WaitForSeconds(1.5f);
        actionToTake();
    }

    private void PlayerLevelUp(int numTimes)
    {
        mNumberOfTimesLeveledUp = numTimes;
        mNextState = CombatUiStates.LevelUp;
    }

    private void DisplayLevelUpPanel()
    {
        RewardsPanel.SetActive(false);
        LevelUpPanel.gameObject.SetActive(true);
        LevelUpPanel.Initialize(mNumberOfTimesLeveledUp);
        mNextState = CombatUiStates.Exit;
    }

    private void DisplayRewardsPanel()
    {
        mNextState = CombatUiStates.Exit;

        Player p = mGlobalGameManager.GetCurrentPlayer();

        BattleEndStateImage.gameObject.SetActive(false);
        RewardsPanel.SetActive(true);

        GoldText.text = $"{mEnemy.GoldGained} G";
        XpText.text = $"{mEnemy.XPGained}";

        p.AddGold((long)mEnemy.GoldGained);
        p.AddExperience(mEnemy.XPGained);

        Item item = mEnemy.GetDrop();
        if (item != null)
        {
            p.AddItem(item);
            DropItemText.text = item.Name;
        }
        else
        {
            DropItemObject.SetActive(false);
        }
    }

    private void DisplayPvpVictoryPanel()
    {
        BattleEndStateImage.gameObject.SetActive(false);
        RewardsPanel.SetActive(true);
        mNextState = CombatUiStates.Exit;
        SwitchToNextState();
    }

    private void Defeated()
    {
        BattleEndStateImage.gameObject.SetActive(false);
        mNextState = CombatUiStates.Exit;
        SwitchToNextState();
    }

    private void ExitBattle()
    {
        mGlobalGameManager.SwitchToBoardScene();
    }

    private void OnDestroy()
    {
        mGlobalGameManager.GetCurrentPlayer().OnPlayerLevelUp -= PlayerLevelUp;
    }
}
