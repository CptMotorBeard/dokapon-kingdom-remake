﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class HealthbarColor
{
    [Range(0, 100)]
    public float HealthPercentage;
    public Color PercentageColor;
}

public class CombatPlayerUiManager : MonoBehaviour
{
    public Slider HpSlider;
    public Image HpFill;

    public TextMeshProUGUI LevelBlock;
    public TextMeshProUGUI NameBlock;

    public TextMeshProUGUI MaxHpBlock;
    public TextMeshProUGUI CurrentHpBlock;

    public TextMeshProUGUI AttackStatBlock;
    public TextMeshProUGUI DefenseStatBlock;
    public TextMeshProUGUI MagicStatBlock;
    public TextMeshProUGUI SpeedStatBlock;

    public TextMeshProUGUI TopSkill;
    public TextMeshProUGUI LeftSkill;
    public TextMeshProUGUI RightSkill;
    public TextMeshProUGUI BottomSkill;

    public GameObject TopSkillUnavailable;
    public GameObject LeftSkillUnavailable;
    public GameObject RightSkillUnavailable;
    public GameObject BottomSkillUnavailable;

    public Image TurnIndicator;
    public Sprite AttackersTurn;
    public Sprite DefendersTurn;

    public bool HealthBarsAnimating;

    CombatUiManager mCombatUiManager;
    CombatCharacter mCharacter;
    float mUiHp;
    float mHpDiff = 0;

    public void InitializeUi(CombatCharacter _character, CombatManager.Action actionTaking)
    {
        mCombatUiManager = CombatUiManager.Instance;

        mCharacter = _character;
        mUiHp = mCharacter.CurHp;

        LevelBlock.text = $"{_character.CharacterBackend.Level}";
        NameBlock.text = _character.CharacterBackend.CharacterName;

        MaxHpBlock.text = $"{_character.MaxHp}";
        CurrentHpBlock.text = $"{_character.CurHp} /";

        UpdateHealth(_character.CurHp);

        AttackStatBlock.text = _character.Attack.ToString("D3");
        DefenseStatBlock.text = _character.Defense.ToString("D3");
        MagicStatBlock.text = _character.Magic.ToString("D3");
        SpeedStatBlock.text = _character.Speed.ToString("D3");

        TurnStart(actionTaking);
    }

    private void Update()
    {
        if (mCharacter != null)
        {
            if (HealthBarsAnimating)
            {
                float dir = Mathf.Sign(mCharacter.CurHp - mUiHp);

                if (mHpDiff == 0)
                {
                    mHpDiff = mCharacter.CurHp - mUiHp;
                    mHpDiff *= dir;
                }

                mUiHp += dir * Time.deltaTime * mHpDiff;

                if (dir < 0)
                    mUiHp = Mathf.Max(mUiHp, mCharacter.CurHp);
                else
                    mUiHp = Mathf.Min(mUiHp, mCharacter.MaxHp);

                if (mUiHp == mCharacter.CurHp)
                {
                    mHpDiff = 0;
                    HealthBarsAnimating = false;
                }

                UpdateHealth(mUiHp);
            }
        }
    }

    public void UpdateHealth(float targetHp)
    {
        float percent = targetHp / mCharacter.MaxHp;
        int intPercent = (int)(percent * 100);

        Color c = CombatUiManager.Instance.FullhealthColor.PercentageColor;

        if (intPercent < mCombatUiManager.FullhealthColor.HealthPercentage)
        {
            if (intPercent < mCombatUiManager.MissingHealthColor.HealthPercentage)
            {
                if (intPercent < mCombatUiManager.LowHealthColor.HealthPercentage)
                {
                    c = mCombatUiManager.LowHealthColor.PercentageColor;
                }
                else
                {
                    c = mCombatUiManager.MissingHealthColor.PercentageColor;
                }
            }
        }

        HpSlider.value = percent;
        HpFill.color = c;
        CurrentHpBlock.text = $"{(int)targetHp} /";
    }

    public void TurnStart(CombatManager.Action newAction)
    {
        if (newAction == CombatManager.Action.AttackAction)
        {
            TurnIndicator.sprite = AttackersTurn;
            SetupAttackSkills();
        }
        else
        {
            TurnIndicator.sprite = DefendersTurn;
            SetupDefenseSkills();
        }
    }

    private void SetupAttackSkills()
    {        
        LeftSkill.text = "Strike";
        RightSkill.text = "Attack";
        BottomSkill.text = "Skill";

        BaseMagic magic = mCharacter.CharacterBackend.OffensiveSpell();
        if (magic != null)
        {
            TopSkillUnavailable.SetActive(false);
            TopSkill.text = magic.Name;
        }
        else
        {
            TopSkillUnavailable.SetActive(true);
            TopSkill.text = "";
        }
    }

    private void SetupDefenseSkills()
    {        
        LeftSkill.text = "Counter";
        RightSkill.text = "Defend";
        BottomSkill.text = "Give Up";

        BaseDefenseMagic magic = mCharacter.CharacterBackend.DefenseSpell();
        if (magic != null)
        {
            TopSkillUnavailable.SetActive(false);
            TopSkill.text = magic.Name;
        }
        else
        {
            TopSkillUnavailable.SetActive(true);
            TopSkill.text = "";
        }
    }
}
