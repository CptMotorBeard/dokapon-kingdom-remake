﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game/Variable/String")]
public class StringVariableSO : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    [SerializeField]
    private string mValue;

    public string Value
    {
        get { return mValue; }
        set { mValue = value; }
    }
}