﻿using UnityEngine;

public class GameEventTrigger : MonoBehaviour
{
    public GameEvent Event;

    public void TriggerEvent()
    {
        Event.Raise();
    }
}
