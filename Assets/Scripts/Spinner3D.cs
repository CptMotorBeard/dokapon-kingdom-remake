﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using UnityEngine;

public class Spinner3D : MonoBehaviour
{
    public delegate void Spinner3DStopDelegate(int index);
    public Spinner3DStopDelegate OnSpinnerStop;

    public float ZDistance = 6.0f;
    public float SpinSpeed = 1500;
    public Transform SpinnerArrow;

    Vector3 mCenter;
    int mSpinnerIndex = 0;
    int mTargetNumber = 0;

    enum Spinner3DState
    {
        Waiting,
        Spinning,
        SlowingDown,
        PickingNumber
    }

    Spinner3DState mState = Spinner3DState.Waiting;

    public void StartSpinner(int targetNumber, int spinnerIndex, int numberOfSpinners)
    {
        mCenter.x = Screen.width / 2f;
        mCenter.y = Screen.height / 2f;
        mCenter.z = ZDistance;

        mTargetNumber = targetNumber;
        mSpinnerIndex = spinnerIndex;

        SetSpinnerLocation(numberOfSpinners);

        mState = Spinner3DState.Spinning;
    }

    float mCurrentSpinSpeed = 0;
    float mTimeElapsed = 0;
    float mTimeToFinish = 0;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (mState == Spinner3DState.Spinning)
            {
                mState = Spinner3DState.SlowingDown;
                mCurrentSpinSpeed = SpinSpeed;
            }
        }

        if (mState == Spinner3DState.Spinning)
        {
            var curRotation = SpinnerArrow.eulerAngles;
            curRotation.z -= SpinSpeed * Time.deltaTime;
            SpinnerArrow.eulerAngles = curRotation;
        }        

        if (mState == Spinner3DState.SlowingDown)
        {
            var curRotation = SpinnerArrow.eulerAngles;
            curRotation.z -= mCurrentSpinSpeed * Time.deltaTime;            
            SpinnerArrow.eulerAngles = curRotation;

            mCurrentSpinSpeed -= SpinSpeed * 0.25f * Time.deltaTime;
            if (mCurrentSpinSpeed < 150)
            {
                mState = Spinner3DState.PickingNumber;
                float targetRotationAmount = SpinnerArrow.eulerAngles.z - TargetRotationForNumber(mTargetNumber);
                targetRotationAmount = targetRotationAmount < 0 ? targetRotationAmount + 360 : targetRotationAmount;

                mTimeElapsed = 0;
                mTimeToFinish = targetRotationAmount / 150;
            }
        }

        if (mState == Spinner3DState.PickingNumber)
        {
            if (mTimeElapsed < mTimeToFinish)
            {
                var curRotation = SpinnerArrow.eulerAngles;
                curRotation.z -= 150 * Time.deltaTime;
                SpinnerArrow.eulerAngles = curRotation;

                mTimeElapsed += Time.deltaTime;
            }
            else
            {
                mState = Spinner3DState.Waiting;
                SpinnerArrow.eulerAngles = new Vector3(SpinnerArrow.eulerAngles.x, SpinnerArrow.eulerAngles.y, TargetRotationForNumber(mTargetNumber));

                StartCoroutine(DelayStopSpinner());
            }
        }
    }

    private IEnumerator DelayStopSpinner()
    {
        yield return new WaitForSeconds(1.0f);
        OnSpinnerStop?.Invoke(mSpinnerIndex);
    }

    private void SetSpinnerLocation(int totalNumberOfSpinners)
    {
        float mod = (Screen.width / 1920f);
        float scaleModifier = (0.65f - (0.05f * (totalNumberOfSpinners - 1))) / mod;
        float radius = (270 + (totalNumberOfSpinners * 25 * scaleModifier)) * mod;
        if (totalNumberOfSpinners <= 1)
        {
            radius = 0;
        }

        float angle = 360f / totalNumberOfSpinners / 2f;        
        float angleIncrease = 360f / totalNumberOfSpinners;
        angle += angleIncrease * mSpinnerIndex;

        float radAngle = angle * Mathf.Deg2Rad;

        float x = radius * Mathf.Sin(radAngle) + mCenter.x;
        float y = radius * Mathf.Cos(radAngle) + mCenter.y;

        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x, y, mCenter.z));
        transform.localScale = Vector3.one * scaleModifier;
    }

    private float TargetRotationForNumber(int number)
    {
        switch (number)
        {
            case 0:
                return 0;
            case 1:
                return 40;
            case 2:
                return 94;
            case 3:
                return 155;
            case 4:
                return 205;
            case 5:
                return 266;
            case 6:
                return 320;
            default:
                return 0;
        }
    }
}