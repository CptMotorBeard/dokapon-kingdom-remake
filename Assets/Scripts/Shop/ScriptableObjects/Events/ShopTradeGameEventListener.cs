﻿using UnityEngine;
using UnityEngine.Events;

public class ShopTradeGameEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public ShopTradeGameEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityItemEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Item tradeItem)
    {
        Response.Invoke(tradeItem);
    }
}

[System.Serializable]
public class UnityItemEvent : UnityEvent<Item> { };