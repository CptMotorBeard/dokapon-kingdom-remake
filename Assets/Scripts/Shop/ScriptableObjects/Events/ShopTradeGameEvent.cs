﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Shop/Trade Event")]
public class ShopTradeGameEvent : ScriptableObject
{    
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<ShopTradeGameEventListener> eventListeners =
        new List<ShopTradeGameEventListener>();

    public void Raise(Item tradeItem)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(tradeItem);
    }

    public void RegisterListener(ShopTradeGameEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(ShopTradeGameEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
