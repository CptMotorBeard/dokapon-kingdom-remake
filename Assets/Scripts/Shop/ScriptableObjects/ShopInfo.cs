﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Shop/Shop Items")]
public class ShopInfo : ScriptableObject
{
    public ShopType Shop;
    public List<Item> AvailableItems;
}