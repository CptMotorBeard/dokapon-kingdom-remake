﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public enum ShopType
{
    Item,
    Weapon,
    Magic
}

public class ShopManager : MonoBehaviour
{
    public const float kSellPercent = 0.5f;
    public Player ActivePlayer { get; private set; }

    public GameObject TooManyItemsPopup;
    public GameObject SwapEquipment;

    public ShopInfo CurrentShopItems;

    GlobalGameManager mGlobalGameManager;

    private void Start()
    {
        if (mGlobalGameManager == null)
            mGlobalGameManager = GlobalGameManager.Instance;
    }

    public void Initialize(List<Item> shopItems, ShopType shop)
    {
        mGlobalGameManager = GlobalGameManager.Instance;
        ActivePlayer = mGlobalGameManager.GetCurrentPlayer();

        CurrentShopItems.AvailableItems = shopItems;
    }

    public void ExitShop()
    {
        mGlobalGameManager.SwitchToBoardScene();
    }

    public void PurchaseItem(Item item)
    {         
        if (ActivePlayer == null)
        {
            Debug.LogError("Current player is null");
            return;
        }

        if (item.IsEquipment && ActivePlayer.Equipment[item.Type] != null)
        {
            long price = item.Price - Mathf.FloorToInt(ActivePlayer.Equipment[item.Type].Price * kSellPercent);
            if (ActivePlayer.TotalGold >= price)
            {
                SwapEquipment s = Instantiate(SwapEquipment, transform).GetComponent<SwapEquipment>();
                s.Initialize(ActivePlayer.Equipment[item.Type] as Equipment, item as Equipment, true, kSellPercent);
                s.OnConfirmationClicked += (shouldSwap) =>
                {
                    if (shouldSwap)
                    {
                        ActivePlayer.AddItem(item);
                        ActivePlayer.AddGold(-price);
                    }
                };
            }
        }
        else if (ActivePlayer.CanPurchaseItem(item))
        {            
            Item tooManyItems = ActivePlayer.PurchaseItem(item);
            Debug.Log($"Purchase: {item.Name}");

            if (tooManyItems)
            {
                TooManyItems tmi = Instantiate(TooManyItemsPopup, transform).GetComponent<TooManyItems>();
                tmi.Initialize(ActivePlayer, new List<Item>() { tooManyItems });
                tmi.OnInventoryClear += SellItems;
            }
        }
        else
        {
            Debug.Log($"Can't purchase: {item.Name}");
            // Error popup
        }
    }

    public void SellItem(Item item)
    {
        if (ActivePlayer == null)
        {
            Debug.LogError("Current player is null");
            return;
        }

        ActivePlayer.TrySell(item, kSellPercent);
    }

    public void SellItems(List<Item> items)
    {
        foreach (var item in items)
        {
            ActivePlayer.ForceSell(item, kSellPercent);
        }
    }
}
