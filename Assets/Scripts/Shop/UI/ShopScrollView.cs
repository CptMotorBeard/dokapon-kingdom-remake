﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopScrollView : MonoBehaviour
{
    public RectTransform SpawnParent;
    public GameObject ShopItemPrefab;
    public float ShopItemPrefabHeight = 100;

    [Header("Purchase Events")]
    public ShopTradeGameEvent BuyEvent;
    public ShopTradeGameEvent SellEvent;

    private List<Item> ShopItems;

    public void Initialize(List<Item> shopItems, bool buying=true, float pricePercent = 1)
    {
        ClearItems();
        ShopItems = shopItems;

        foreach (Item item in ShopItems)
        {
            if (item == null)
                continue;

            GameObject shopItem = Instantiate(ShopItemPrefab, SpawnParent);

            Image[] itemImage = shopItem.GetComponentsInChildren<Image>();
            itemImage[1].sprite = item.ItemSprite;

            TextMeshProUGUI[] prefabText = shopItem.GetComponentsInChildren<TextMeshProUGUI>();
            prefabText[0].text = item.Name;
            prefabText[1].text = $"{Mathf.FloorToInt(item.Price * pricePercent)} G";

            Button b = shopItem.GetComponentInChildren<Button>();

            if (buying)
            {
                b.onClick.AddListener(delegate { BuyEvent.Raise(item); });
            }
            else
            {
                b.onClick.AddListener(delegate { SellEvent.Raise(item); });
            }            
        }

        SpawnParent.sizeDelta = new Vector2(SpawnParent.rect.width, ShopItemPrefabHeight * ShopItems.Count);
    }

    private void ClearItems()
    {
        for (int i = SpawnParent.childCount - 1; i >= 0; --i)
        {
            Destroy(SpawnParent.GetChild(i).gameObject);
        }
    }
}
