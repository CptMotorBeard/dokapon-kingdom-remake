﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Linq;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShopUiManager : MonoBehaviour
{
    public ShopScrollView ItemPanel;    
    public ShopInfo CurrentShopItems;
    public TextMeshProUGUI PlayerGold;

    [Header("Item Panel")]
    public RectTransform Panel;
    public float InitialX;
    public float DestX;

    [Header("Animation Events")]
    public GameEvent BuyPanelAnimateInEvent;
    public GameEvent BuyPanelAnimateOutEvent;

    public string CurrentSellingInventory { get; private set; } = "";    

    bool bItemPanelOpen = false;

    public const string kItemInventory = "item";
    public const string kMagicInventory = "magic";
    public const string kEquipment = "equip";
    
    public void OpenSellPanelInventory() { OpenSellPanel(kItemInventory); }
    public void OpenSellPanelMagic() { OpenSellPanel(kMagicInventory); }
    public void OpenSellPanelEquipment() { OpenSellPanel(kEquipment); }
    public void OpenSellPanel(string inventoryType, bool animateIn = true)
    {
        bItemPanelOpen = true;

        CurrentSellingInventory = inventoryType;

        List<Item> sellables = new List<Item>();
        Player player = GlobalGameManager.Instance.GetCurrentPlayer();

        if (CurrentSellingInventory.Equals(kItemInventory))
        {
            sellables = player.ItemInventory.ToList();
        }
        else if (CurrentSellingInventory.Equals(kMagicInventory))
        {
            sellables = player.MagicInventory.ToList();
        }
        else if (CurrentSellingInventory.Equals(kEquipment))
        {
            sellables = player.Equipment.Values.ToList();
        }

        ItemPanel.Initialize(sellables, buying: false, pricePercent: ShopManager.kSellPercent);

        if (animateIn)
        {
            AnimateInPanel();
        }
    }

    public void OpenBuyPanel()
    {
        bItemPanelOpen = true;
        ItemPanel.Initialize(CurrentShopItems.AvailableItems);

        AnimateInPanel();
    }

    public void CloseItemPanel()
    {
        if (bItemPanelOpen)
        {
            bItemPanelOpen = false;

            AnimateOutPanel();
        }
    }

    public void AnimateInPanel()
    {
        BuyPanelAnimateInEvent.Raise();
        LeanTween.moveX(Panel, DestX, 0.5f).setEase(LeanTweenType.linear);
    }

    public void AnimateOutPanel()
    {
        BuyPanelAnimateOutEvent.Raise();
        LeanTween.moveX(Panel, InitialX, 0.5f).setEase(LeanTweenType.linear);
    }
}
