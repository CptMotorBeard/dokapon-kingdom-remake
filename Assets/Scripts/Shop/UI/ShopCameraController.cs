﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class ShopCameraController : MonoBehaviour
{
    public Vector3 StartLocation;
    public Vector3 EndLocation;

    public Vector3 StartRotation;
    public Vector3 EndRotation;

    public GameEvent ShopCameraSlideInEventEnd;
    public GameEvent ShopCameraSlideOutEventEnd;
    public GameEvent ShopCameraRotateInEventEnd;
    public GameEvent ShopCameraRotateOutEventEnd;

    public void Start()
    {
        SlideCameraIn();
    }

    private void SlideCameraIn()
    {
        transform.position = StartLocation;
        LeanTween.move(gameObject, EndLocation, 1f).setEase(LeanTweenType.linear).setOnComplete(ShopCameraSlideInEventEnd.Raise);
    }

    public void SlideCameraOut()
    {
        transform.position = EndLocation;
        LeanTween.move(gameObject, StartLocation, 1f).setEase(LeanTweenType.linear).setOnComplete(ShopCameraSlideOutEventEnd.Raise);
    }

    public void RotateCameraIn()
    {        
        LeanTween.rotate(gameObject, EndRotation, 0.5f).setEase(LeanTweenType.linear).setOnComplete(ShopCameraRotateInEventEnd.Raise);
    }

    public void RotateCameraOut()
    {
        LeanTween.rotate(gameObject, StartRotation, 0.5f).setEase(LeanTweenType.linear).setOnComplete(ShopCameraRotateOutEventEnd.Raise);
    }
}
