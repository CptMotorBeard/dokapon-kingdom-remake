﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalGameManager : MonoBehaviour
{
    public static GlobalGameManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public const float kTransitionDelay = 0.9f;
    public const string kBattleScene = "Battle";
    public const string kBoardScene = "Board";
    public const string kShopScene = "Shop";
    public const string kTownScene = "TownScene";

    const int kDaysInAWeek = 7;

    public int NumberOfPlayers { get; private set;} = 4;
    public int CurrentPlayerTurn { get; private set; } = 0;

    public GameObject PlayerPrefab;
    public Material[] PlayerMaterial;    
    public Vector3 PlayerStartingPosition;

    public StartingPlayerJob[] DEBUG_JOBS;
    public string[] DEBUG_NAMES;
    public List<Item> DEBUG_STARTINVENTORY;

    public GameEvent FadeInTransitionEvent;
    public GameEvent FadeOutTransitionEvent;

    public ShopInfo CurrentShopItems;

    public int Date { get; private set; } = 0;
    public int Week { get; private set; } = 1;

    public GameFlowManager FlowManager;

    Player[] mAllPlayers;
    List<Town> mAllTowns;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == kBoardScene)
        {
            NumberOfPlayers = DEBUG_JOBS.Length;
            StartNewBoard(DEBUG_NAMES, DEBUG_JOBS);
        }
        else if (SceneManager.GetActiveScene().name == kShopScene)
        {
            mAllPlayers = new Player[1];
            Player p = new Player(DEBUG_JOBS[0], "Name");

            var inventoryItem = ScriptableObject.CreateInstance(typeof(ItemCrystal)) as ItemCrystal;
            inventoryItem.Name = "TEST";
            inventoryItem.Price = 100;

            var equipment = ScriptableObject.CreateInstance(typeof(Weapon)) as Weapon;
            equipment.Name = "DA SWORD";
            equipment.Price = 5000;

            p.AddItems(new List<Item> { inventoryItem, inventoryItem, inventoryItem });
            p.Equip(equipment);

            p.AddGold(999999);

            mAllPlayers[0] = p;
        }
        else if (SceneManager.GetActiveScene().name == kBattleScene)
        {
            mAllPlayers = new Player[1];
            Player p = new Player(DEBUG_JOBS[0], "Name");
            mAllPlayers[0] = p;

            CombatManager.Instance.DEBUG_MODE = true;
        }
    }

    #region Initialization
    private void StartNewBoard(string[] names, StartingPlayerJob[] startingPlayerJobs)
    {        
        mAllPlayers = new Player[NumberOfPlayers];
        mAllTowns = new List<Town>();

        PlayerMapData[] initialMapData = new PlayerMapData[NumberOfPlayers];

        for (int i = 0; i < NumberOfPlayers; i++)
        {
            var mapData = new PlayerMapData
            {
                Location = PlayerStartingPosition
            };

            initialMapData[i] = mapData;

            mAllPlayers[i] = new Player(startingPlayerJobs[i], names[i]);
            mAllPlayers[i].AddItems(DEBUG_STARTINVENTORY);
        }

        FlowManager = new GameFlowManager();
        FlowManager.Initialize(initialMapData);

        var townNodes = FindObjectsOfType<TownNode>();
        foreach (var node in townNodes)
        {
            Town t = new Town(node.TownInformation);

            int total = node.TownInformation.PossibleBossMonsters.Length;
            EnemyStats boss = node.TownInformation.PossibleBossMonsters[UnityEngine.Random.Range(0, total)];
            t.BossMonster = new Character(boss);
            
            node.DisplayPanel.Initialize(t);
            node.Initialize(t.BossMonster);

            mAllTowns.Add(t);
        }
    }

    private void SetupBoard()
    {
        PlayerMapData[] mapData = new PlayerMapData[NumberOfPlayers];
        for (int i = 0; i < NumberOfPlayers; i++)
        {
            mapData[i] = mAllPlayers[i].MapData;
        }

        FlowManager.SetupBoard(mapData);

        var townNodes = FindObjectsOfType<TownNode>();
        foreach (var node in townNodes)
        {
            var t = GetTownByStats(node.TownInformation);
            node.Initialize();
            node.DisplayPanel.Initialize(t);
        }
    }

    public delegate void OnBattleStartDelegate();
    public OnBattleStartDelegate OnBattleStart;
    private void InitializeBattle(Character right, int pvpIndex=-1)
    {
        var cm = CombatManager.Instance;
        if (cm != null)
        {
            if (pvpIndex >= 0)
                cm.Initialize(mAllPlayers[CurrentPlayerTurn], right, PlayerMaterial[CurrentPlayerTurn], PlayerMaterial[pvpIndex], true);
            else
                cm.Initialize(mAllPlayers[CurrentPlayerTurn], right, PlayerMaterial[CurrentPlayerTurn]);

            OnBattleStart?.Invoke();
        }
    }

    private void InitializeTown(Town town)
    {
        var tm = TownManager.Instance;
        if (tm != null)
        {
            tm.Initialize(town);
        }
    }

    private void InitializeShop(List<Item> shopItems, ShopType shop)
    {
        CurrentShopItems.AvailableItems = shopItems;
        CurrentShopItems.Shop = shop;
    }
    #endregion

    #region Character and Town Information
    public Color GetPlayerColor(Player p)
    {
        int i = mAllPlayers.ToList().IndexOf(p);

        if (i >= 0 && i < PlayerMaterial.Length)
        {
            return PlayerMaterial[i].color;
        }

        return Color.black;
    }

    public int GetPlayerRanking(int index)
    {
        int i = Array.IndexOf(mAllPlayers.OrderBy((player) => { return player.TotalValue; }).Reverse().ToArray(), mAllPlayers[index]);
        
        if (i >= mAllPlayers.Length || i < 0)
        {
            return 3;
        }
        else
        {
            return i;
        }
    }

    public int GetCurrentPlayerRanking()
    {
        return GetPlayerRanking(CurrentPlayerTurn);
    }

    public Player[] GetOtherPlayers()
    {
        return GetOtherPlayers(CurrentPlayerTurn);
    }

    public Player GetPlayer(int index)
    {
        if (index < 0 || index > NumberOfPlayers)
        {
            Debug.LogError($"Player {index} doesn't exist");
            return null;
        }

        return mAllPlayers[index];
    }

    private Player[] GetOtherPlayers(int player)
    {
        Player[] players = new Player[NumberOfPlayers - 1];
        int index = 0;

        foreach (Player p in mAllPlayers)
        {
            if (p != mAllPlayers[player])
            {
                players[index++] = p;
            }
        }

        return players;
    }

    public Player GetCurrentPlayer()
    {
        return GetPlayer(CurrentPlayerTurn);
    }

    public Town GetTownInformation(TownStats town)
    {
        foreach (Town t in mAllTowns)
        {
            if (t.Stats == town)
            {
                return t;
            }
        }

        Town newTown = new Town(town);
        mAllTowns.Add(newTown);

        return newTown;
    }

    public void DefeatTownMonster(TownStats town)
    {
        foreach (Town t in mAllTowns)
        {
            if (t.Stats == town)
            {
                t.Owner = mAllPlayers[CurrentPlayerTurn];
                t.BossMonster = null;
                return;
            }
        }

        Town newTown = new Town(town);
        newTown.Owner = mAllPlayers[CurrentPlayerTurn];
        mAllTowns.Add(newTown);
    }

    private Town GetTownByStats(TownStats stats)
    {
        foreach (Town t in mAllTowns)
        {
            if (t.Stats == stats)
            {
                return t;
            }
        }

        return null;
    }
    #endregion

    #region Scenes
    public void StartGame(int numberOfPlayers, string[] names, StartingPlayerJob[] startingPlayerJobs)
    {
        NumberOfPlayers = numberOfPlayers;
        StartCoroutine(TransitionToStartGame(names, startingPlayerJobs));
    }

    public void SwitchToBattleScene(CharacterStats right)
    {
        Character newChar = new Character(right);
        StartCoroutine(TransitionToBattle(newChar));
    }

    public void SwitchToBattleScene(Character right)
    {
        StartCoroutine(TransitionToBattle(right));
    }

    public void SwitchToBattleScene(int pvpIndex)
    {
        StartCoroutine(TransitionToBattle(GetPlayer(pvpIndex), pvpIndex));
    }

    public void SwitchToBoardScene()
    {
        StartCoroutine(TransitionToBoard());
    }

    public void SwitchToTownScene(Town town)
    {
        StartCoroutine(TransitionToTown(town));
    }

    public void SwitchToShopScene(List<Item> shopItems, ShopType shop)
    {
        StartCoroutine(TransitionToShop(shopItems, shop));
    }

    private IEnumerator TransitionToStartGame(string[] names, StartingPlayerJob[] startingPlayerJobs)
    {
        yield return TransitionScenes(kBoardScene);
        StartNewBoard(names, startingPlayerJobs);

        yield return new WaitForSeconds(kTransitionDelay);
    }

    private IEnumerator TransitionToBoard()
    {
        yield return TransitionScenes(kBoardScene);

        SetupBoard();
        TurnComplete();

        yield return new WaitForSeconds(kTransitionDelay);
    }

    private IEnumerator TransitionToBattle(Character right, int pvpIndex=-1)
    {
        yield return TransitionScenes(kBattleScene);

        InitializeBattle(right, pvpIndex);

        yield return new WaitForSeconds(kTransitionDelay);
    }

    private IEnumerator TransitionToTown(Town town)
    {
        yield return TransitionScenes(kTownScene);

        InitializeTown(town);

        yield return new WaitForSeconds(kTransitionDelay);
    }

    private IEnumerator TransitionToShop(List<Item> shopItems, ShopType shop)
    {
        yield return TransitionScenes(kShopScene);

        InitializeShop(shopItems, shop);

        yield return new WaitForSeconds(kTransitionDelay);
    }

    private IEnumerator TransitionScenes(string scene)
    {
        Scene s = SceneManager.GetActiveScene();
        
        FadeOutTransitionEvent.Raise();
        
        yield return new WaitForSeconds(kTransitionDelay);
        SceneManager.LoadScene(scene);

        while (s == SceneManager.GetActiveScene())
        {
            yield return new WaitForEndOfFrame();
        }

        FadeInTransitionEvent.Raise();
    }
    #endregion

    public delegate void TurnCompleteDelegate();
    public TurnCompleteDelegate OnTurnComplete;

    public void TurnComplete()
    {
        mAllPlayers[CurrentPlayerTurn].TurnPass();

        CurrentPlayerTurn += 1;
        CurrentPlayerTurn %= NumberOfPlayers;
        if (CurrentPlayerTurn == 0)
        {
            Date++;
            Date %= kDaysInAWeek;

            foreach (Player p in mAllPlayers)
            {
                p.DayPass();
            }

            if (Date == 0)
            {
                Week++;

                foreach (Player p in mAllPlayers)
                {
                    p.WeekPass();
                }
            }
        }

        OnTurnComplete?.Invoke();
    }

    public void SaveBoardState(PlayerMapData[] playerMapData)
    {
        SavePlayerMapData(playerMapData);
    }

    private void SavePlayerMapData(PlayerMapData[] playerMapData)
    {
        if (playerMapData.Length != mAllPlayers.Length)
            Debug.LogError("Passed invalid array size for number of players");

        for (int i = 0; i < NumberOfPlayers; i++)
        {
            mAllPlayers[i].MapData = playerMapData[i];
        }
    }

    public void QuitGame()
    {
        Application.Quit();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
