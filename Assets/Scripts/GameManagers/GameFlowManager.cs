﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameFlowManager
{
    public PlayerUi Ui;
    private PlayerController[] Players;
    private Vector3 mCurrentPlayerNodePos;
    private TownStats mCurrentPlayerTown;

    private int mNumberOfSpaces;

    GlobalGameManager mGlobalGameManager;
    BattleNodeManager mBattleNodeManager;

    public void Initialize(PlayerMapData[] playerMapData)
    {
        mGlobalGameManager = GlobalGameManager.Instance;
        mBattleNodeManager = new BattleNodeManager();

        SetupBoard(playerMapData);

        TurnStart();
    }

    public void SetupBoard(PlayerMapData[] playerMapData)
    {
        int numberOfPlayers = mGlobalGameManager.NumberOfPlayers;

        Players = new PlayerController[numberOfPlayers];
        for (int i = 0; i < numberOfPlayers; i++)
        {
            GameObject g = Object.Instantiate(mGlobalGameManager.PlayerPrefab, playerMapData[i].Location, Quaternion.identity);

            PlayerCustomizationController custom = g.GetComponent<PlayerCustomizationController>();
            custom.SwapMaterials(mGlobalGameManager.PlayerMaterial[i]);

            if (mGlobalGameManager.GetPlayer(i).CurHp <= 0)
            {
                custom.Die();
            }

            Players[i] = g.GetComponent<PlayerController>();
        }

        Ui = PlayerUi.Instance;
        Ui.gameObject.SetActive(true);
    }

    public void TurnStart()
    {
        mGlobalGameManager.OnTurnComplete -= TurnStart;
        Players[mGlobalGameManager.CurrentPlayerTurn].OnTurnStart();

        Player p = mGlobalGameManager.GetCurrentPlayer();
        if (p.CurHp <= 0)
        {
            if (p.IsDead)
            {
                --p.DeathTimer;
                if (p.DeathTimer <= 0)
                {
                    Players[mGlobalGameManager.CurrentPlayerTurn].GetComponent<PlayerCustomizationController>().Live();
                    p.Heal(p.CharacterStats[CharacterStat.Hp]);
                    Ui.Initialize();
                }
                else
                {
                    Ui.DisplayDeathTimer(p.CharacterName, p.DeathTimer);
                    Ui.OnDeathConfirmed += ForcePlayerTurnEnd;
                }
            }
            else
            {
                p.IsDead = true;
                p.DeathTimer = Random.Range(1, 4);
                Players[mGlobalGameManager.CurrentPlayerTurn].GetComponent<PlayerCustomizationController>().Die();
                Ui.DisplayDeathTimer(p.CharacterName, p.DeathTimer);
                Ui.OnDeathConfirmed += ForcePlayerTurnEnd;
            }
        }
        else
        {
            RoutingNode r = Players[mGlobalGameManager.CurrentPlayerTurn].GetCurrentNode();
            BattleNode b = r.GetComponent<BattleNode>();
            if (b != null)
            {
                bool inPvp = false;

                b.ClearPvP();

                for (int i = 0; i < mGlobalGameManager.NumberOfPlayers; i++)
                {
                    if (Players[i].GetCurrentNode() == r)
                    {
                        if (i != mGlobalGameManager.CurrentPlayerTurn && mGlobalGameManager.GetPlayer(i).CurHp > 0)
                        {
                            inPvp = true;
                            break;
                        }
                    }
                }

                if (inPvp || (r != null && mBattleNodeManager.CharactersOnNode(r.transform.position) != null))
                {
                    PlayerTurnEnd(r);
                }
                else
                {
                    Ui.Initialize();
                }
            }
            else
            {
                Ui.Initialize();
            }
        }     
    }

    public void SpinnerStop(int numberOfSpaces)
    {
        mNumberOfSpaces = numberOfSpaces;

        if (mNumberOfSpaces <= 0)
        {
            ConfirmEndTurn(Players[mGlobalGameManager.CurrentPlayerTurn].GetCurrentNode(), true);
            return;
        }

        mPlayerDirections.Clear();
        Players[mGlobalGameManager.CurrentPlayerTurn].AbleToMove = true;
        Players[mGlobalGameManager.CurrentPlayerTurn].OnPlayerMoved += PlayerMoved;
    }

    private List<PathfindingGrid.Direction> mPlayerDirections = new List<PathfindingGrid.Direction>();
    private void PlayerMoved(PathfindingGrid.Direction newDirection)
    {
        if (mPlayerDirections.Count > 0 && (mPlayerDirections[mPlayerDirections.Count - 1] == newDirection.Reverse()))
        {
            mNumberOfSpaces++;
            mPlayerDirections.RemoveAt(mPlayerDirections.Count - 1);
        }
        else
        {
            mPlayerDirections.Add(newDirection);
            mNumberOfSpaces--;
            
            if (mNumberOfSpaces <= 0)
            {
                Players[mGlobalGameManager.CurrentPlayerTurn].AbleToMove = false;
                Players[mGlobalGameManager.CurrentPlayerTurn].OnPlayerArrived += ConfirmEndTurn;
            }
        }

        Ui.UpdateSpaces(mNumberOfSpaces);
    }

    private void ConfirmEndTurn(RoutingNode endNode, bool forcedEnd = false)
    {
        Players[mGlobalGameManager.CurrentPlayerTurn].OnPlayerArrived -= ConfirmEndTurn;

        if (forcedEnd)
        {
            PlayerTurnEnd(endNode);
            return;
        }

        Ui.OnTurnEndConfirmation += TurnEndConfirmationSent;
        Ui.DisplayEndTurnConfirmation(endNode);
    }

    private void TurnEndConfirmationSent(bool endTurn, RoutingNode endNode)
    {
        Ui.OnTurnEndConfirmation -= TurnEndConfirmationSent;

        if (endTurn)
        {
            PlayerTurnEnd(endNode);
        }
        else
        {
            mNumberOfSpaces++;
            PathfindingGrid.Direction lastDir = mPlayerDirections[mPlayerDirections.Count - 1];
            mPlayerDirections.RemoveAt(mPlayerDirections.Count - 1);
            lastDir = lastDir.Reverse();

            Players[mGlobalGameManager.CurrentPlayerTurn].AbleToMove = true;
            Players[mGlobalGameManager.CurrentPlayerTurn].ForceMove(lastDir);
            Ui.UpdateSpaces(mNumberOfSpaces);
        }
    }

    private void PlayerTurnEnd(RoutingNode endNode)
    {
        Debug.Log($"Landed on node: {endNode.NodeType}");

        mGlobalGameManager.OnTurnComplete += TurnStart;

        mCurrentPlayerTown = endNode.GetComponent<TownNode>()?.TownInformation;
        mCurrentPlayerNodePos = endNode.transform.position;
        Players[mGlobalGameManager.CurrentPlayerTurn].OnTurnEnd();
        Players[mGlobalGameManager.CurrentPlayerTurn].OnPlayerMoved -= PlayerMoved;

        var battleNode = endNode.GetComponent<BattleNode>();
        if (battleNode != null)
        {
            battleNode.ClearPvP();

            for (int i = 0; i < mGlobalGameManager.NumberOfPlayers; i++)
            {
                if (Players[i].GetCurrentNode() == endNode)
                {
                    if (i != mGlobalGameManager.CurrentPlayerTurn && mGlobalGameManager.GetPlayer(i).CurHp > 0)
                        battleNode.AddPvpFight(i);
                }
            }

            var character = mBattleNodeManager.CharactersOnNode(mCurrentPlayerNodePos);
            if (character != null)
            {
                battleNode.AddCharacter(character);
            }
        }
        
        endNode.StartCoroutine(EndTurn(endNode));        
    }

    public void ForcePlayerTurnEnd()
    {
        mGlobalGameManager.OnTurnComplete += TurnStart;
        Ui.OnDeathConfirmed -= ForcePlayerTurnEnd;

        Players[mGlobalGameManager.CurrentPlayerTurn].OnTurnEnd();
        mGlobalGameManager.TurnComplete();
    }

    public void StartBattle()
    {
        mGlobalGameManager.OnBattleStart -= StartBattle;
        CombatManager.Instance.OnCombatEnd += BattleEnd;
    }

    public void BattleEnd(Character enemy, bool pvp)
    {
        CombatManager.Instance.OnCombatEnd -= BattleEnd;

        if (!pvp)
        {
            mBattleNodeManager.BattleEnd(mCurrentPlayerNodePos, enemy);            
        }

        if (mCurrentPlayerTown != null && enemy.CurHp <= 0)
        {
            mGlobalGameManager.DefeatTownMonster(mCurrentPlayerTown);
        }
    }

    private IEnumerator EndTurn(RoutingNode endNode)
    {
        EndNode n = endNode.GetComponent<EndNode>();
        if (n != null)
        {
            yield return n.LandedOnNode(Players, Ui, endNode);
        }
    }
}
