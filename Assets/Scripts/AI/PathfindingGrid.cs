﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public class PathfindingGrid : MonoBehaviour
{

    #region Singleton
    public static PathfindingGrid instance;

    void Awake()
    {
        if (instance)
        {
            Debug.LogWarning("Another instance of pathfinding grid already exists");
            return;
        }
        instance = this;
    }
    #endregion

    public Routing routing;

    public LayerMask safeLayer;             // Layer for path
    public Vector2 gridWorldSize;           // Size of pathfinding grid in world units
    public float nodeRadius;                // Size of nodes

    public int blockingFeather;             // The feathering of the blocking nodes
    public float spacing;                   // Spacing between each node - for Gizmos only
    public Color blocked;                   // Set the colours for the Gizmo
    public Color walkable;
    public Color space;

    Node[,] nodeArray;
    float nodeDiameter;
    int gridSizeX;                          // Size of grid in units
    int gridSizeY;

    Vector3 topRight;                     // Store the bottom left of the grid, to transform world position

    void Start()
    {
        nodeDiameter = nodeRadius * 2;

        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);   // Get the size of the grid
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        CreateGrid();
        FindPaths();
    }

    void CreateGrid()
    {
        nodeArray = new Node[gridSizeX, gridSizeY];
        topRight = transform.position + Vector3.right * gridWorldSize.x / 2 + Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPosition = topRight + Vector3.left * (x * nodeDiameter + nodeRadius) + Vector3.back * (y * nodeDiameter + nodeRadius);
                bool blocking = true;

                nodeArray[x, y] = new Node(blocking, worldPosition, x, y);
            }
        }
    }

    void FindPaths()
    {
        foreach (var routingNode in routing.RoutingNodes)
        {
            Node n = NodeFromWorldPoint(routingNode.transform.position);
            if (n == null)
            {
                Debug.LogError("Invalid node for some reason");
                continue;
            }

            n.isBlocker = false;
            n.isSpaceNode = true;
            n.associatedSpace = routingNode;
        }

        foreach (var routingNode in routing.RoutingNodes)
        {
            Node n = NodeFromWorldPoint(routingNode.transform.position);
            if (n == null)
            {
                Debug.LogError("Invalid node for some reason");
                continue;
            }

            int x, y;

            if (routingNode.CanMoveDown)
            {
                x = n.gridX;
                y = n.gridY + 1;

                if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                {
                    Debug.LogError($"{routingNode.gameObject} can not move down");
                }

                // Since all nodes start as blockers, if the node is not a blocker that means a path has already been made to this node from this direction
                if (nodeArray[x, y].isBlocker)
                {
                    while (!nodeArray[x, y].isSpaceNode)
                    {
                        if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                        {
                            Debug.LogError($"{routingNode.gameObject} can not move down");
                            break;
                        }

                        nodeArray[x, y++].isBlocker = false;
                    }
                }                
            }

            if (routingNode.CanMoveLeft)
            {
                x = n.gridX + 1;
                y = n.gridY;

                if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                {
                    Debug.LogError($"{routingNode.gameObject} can not move left");
                }

                // Since all nodes start as blockers, if the node is not a blocker that means a path has already been made to this node from this direction
                if (nodeArray[x, y].isBlocker)
                {
                    while (!nodeArray[x, y].isSpaceNode)
                    {
                        if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                        {
                            Debug.LogError($"{routingNode.gameObject} can not move left");
                            break;
                        }

                        nodeArray[x++, y].isBlocker = false;
                    }
                }                
            }

            if (routingNode.CanMoveRight)
            {
                x = n.gridX - 1;
                y = n.gridY;

                if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                {
                    Debug.LogError($"{routingNode.gameObject} can not move right");
                }

                // Since all nodes start as blockers, if the node is not a blocker that means a path has already been made to this node from this direction
                if (nodeArray[x, y].isBlocker)
                {
                    while (!nodeArray[x, y].isSpaceNode)
                    {
                        if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                        {
                            Debug.LogError($"{routingNode.gameObject} can not move right");
                            break;
                        }

                        nodeArray[x--, y].isBlocker = false;
                    }
                }
            }

            if (routingNode.CanMoveUp)
            {
                x = n.gridX;
                y = n.gridY - 1;

                if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                {
                    Debug.LogError($"{routingNode.gameObject} can not move up");
                }

                // Since all nodes start as blockers, if the node is not a blocker that means a path has already been made to this node from this direction
                if (nodeArray[x, y].isBlocker)
                {
                    while (!nodeArray[x, y].isSpaceNode)
                    {
                        if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
                        {
                            Debug.LogError($"{routingNode.gameObject} can not move up");
                            break;
                        }

                        nodeArray[x, y--].isBlocker = false;
                    }
                }
            }
        }
    }

    public Node NodeFromWorldPoint(Vector3 worldPoint)
    {
        int x = -Mathf.RoundToInt((worldPoint.x - topRight.x - nodeRadius) / nodeDiameter);
        int y = -Mathf.RoundToInt((worldPoint.z - topRight.z - nodeRadius) / nodeDiameter);

        // For some unknown reason, this is always off by 1 in both directions.
        x -= 1;
        y -= 1;

        if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
            return null;

        return nodeArray[x, y];
    }

    public Node GetPathableNode(Vector3 worldPoint)
    {
        List<Node> OPEN = new List<Node>();
        HashSet<Node> CLOSED = new HashSet<Node>();

        Node currentNode = NodeFromWorldPoint(worldPoint);

        if (currentNode == null || !currentNode.isBlocker)
            return currentNode;

        OPEN.Add(currentNode);
        int count = 0;

        while (OPEN.Count > 0 && count < 1000)
        {
            count++;
            currentNode = OPEN[0];

            OPEN.Remove(currentNode);
            CLOSED.Add(currentNode);

            List<Node> neighbours = GetNeighbouringNodes(currentNode);

            foreach (Node n in neighbours)
            {
                if (!n.isBlocker)
                    return n;

                if (!CLOSED.Contains(n))
                {
                    OPEN.Add(n);
                }
            }
        }

        return null;
    }

    public Node NodeAtPosition(int x, int y)
    {
        return nodeArray[x, y];
    }

    public bool BlockingNodeAt(int x, int y)
    {
        if (x < 0 || y < 0 || x > gridSizeX || y > gridSizeY)
            return true;

        return nodeArray[x, y].isBlocker;
    }

    public List<Node> GetNeighbouringNodes(Node node)
    {
        List<Node> neighbours = new List<Node>();

        int checkX, checkY;

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (y != 0 && x == 0 || x != 0 && y == 0)
                {
                    checkX = node.gridX + x;
                    checkY = node.gridY + y;
                    if (checkX > 0 && checkX < gridSizeX)
                    {
                        if (checkY > 0 && checkY < gridSizeY)
                        {
                            neighbours.Add(nodeArray[checkX, checkY]);
                        }
                    }
                }
            }
        }

        return neighbours;
    }

    public enum Direction
    {
        Up = 0,
        Down = 2,
        Left = 3,
        Right = 1,
        None
    }
    public RoutingNode GetDirectedNeighbour(Vector3 currentPosition, Direction dir)
    {
        Node n = NodeFromWorldPoint(currentPosition);
        if (n == null)
        {
            Debug.LogError("Standing on a null point");
            return null;
        }

        int x = 0;
        int y = 0;

        if (dir == Direction.Down)
            y++;
        else if (dir == Direction.Left)
            x++;
        else if (dir == Direction.Right)
            x--;
        else if (dir == Direction.Up)
            y--;

        int localX = n.gridX + x;
        int localY = n.gridY + y;


        if (localX < 0 || localX >= gridSizeX || localY < 0 || localY >= gridSizeY)
        {
            Debug.LogError($"Cannot move in direction {dir}");
        }

        while (!nodeArray[localX, localY].isSpaceNode)
        {
            if (localX < 0 || localX >= gridSizeX || localY < 0 || localY >= gridSizeY)
            {
                Debug.LogError($"Cannot move in direction {dir}");
                break;
            }

            localX += x;
            localY += y;
        }

        return nodeArray[localX, localY].associatedSpace;
    }

    public List<Node> GetDirectedNeighbouringNodes(Node node)
    {
        if (node.parentNode != null)
        {
            List<Node> neighbouringNodes = new List<Node>();

            int dirX = Mathf.Clamp(node.gridX - node.parentNode.gridX, -1, 1);
            int dirY = Mathf.Clamp(node.gridY - node.parentNode.gridY, -1, 1);

            int checkX, checkY;

            if (dirX != 0 && dirY != 0)
            {
                checkX = node.gridX + dirX;
                checkY = node.gridY + dirY;

                if (checkX > 0 && checkX < gridSizeX)
                {
                    if (checkY > 0 && checkY < gridSizeY)
                    {
                        if (!nodeArray[checkX, checkY].isBlocker)
                        {
                            neighbouringNodes.Add(nodeArray[checkX, checkY]);
                        }
                    }
                }

                checkX = node.gridX + dirX;
                checkY = node.gridY;

                if (checkX > 0 && checkX < gridSizeX)
                {
                    if (checkY > 0 && checkY < gridSizeY)
                    {
                        if (!nodeArray[checkX, checkY].isBlocker)
                        {
                            neighbouringNodes.Add(nodeArray[checkX, checkY]);
                        }
                    }
                }

                checkX = node.gridX;
                checkY = node.gridY + dirY;

                if (checkX > 0 && checkX < gridSizeX)
                {
                    if (checkY > 0 && checkY < gridSizeY)
                    {
                        if (!nodeArray[checkX, checkY].isBlocker)
                        {
                            neighbouringNodes.Add(nodeArray[checkX, checkY]);
                        }
                    }
                }
            }
            else
            {
                bool move = false, movePositive = false, moveNegative = false;

                if (dirX != 0)                  // Left / right
                {
                    checkX = node.gridX + dirX;
                    checkY = node.gridY;

                    if (checkX > 0 && checkX < gridSizeX)
                    {
                        move = nodeArray[checkX, checkY].isBlocker;

                        checkY = node.gridY - 1;
                        if (checkY > 0)
                        {
                            moveNegative = nodeArray[checkX, checkY].isBlocker;
                        }

                        checkY = node.gridY + 1;
                        if (checkY < gridSizeY)
                        {
                            movePositive = nodeArray[checkX, checkY].isBlocker;
                        }
                    }

                    if (move)
                    {
                        neighbouringNodes.Add(nodeArray[checkX, node.gridY]);
                        if (moveNegative)
                        {
                            neighbouringNodes.Add(nodeArray[checkX, node.gridY - 1]);
                        }
                        if (movePositive)
                        {
                            neighbouringNodes.Add(nodeArray[checkX, node.gridY + 1]);
                        }
                    }
                    if (moveNegative)
                    {
                        neighbouringNodes.Add(nodeArray[node.gridX, node.gridY - 1]);
                    }
                    if (movePositive)
                    {
                        neighbouringNodes.Add(nodeArray[node.gridX, node.gridY + 1]);
                    }
                }
                else if (dirY != 0)             // Up / down
                {
                    checkX = node.gridX;
                    checkY = node.gridY + dirY;

                    if (checkY > 0 && checkY < gridSizeY)
                    {
                        move = nodeArray[checkX, checkY].isBlocker;

                        checkX = node.gridX - 1;
                        if (checkX > 0)
                        {
                            moveNegative = nodeArray[checkX, checkY].isBlocker;
                        }

                        checkX = node.gridX + 1;
                        if (checkX < gridSizeX)
                        {
                            movePositive = nodeArray[checkX, checkY].isBlocker;
                        }
                    }

                    if (move)
                    {
                        neighbouringNodes.Add(nodeArray[node.gridX, checkY]);
                        if (moveNegative)
                        {
                            neighbouringNodes.Add(nodeArray[node.gridX - 1, checkY]);
                        }
                        if (movePositive)
                        {
                            neighbouringNodes.Add(nodeArray[node.gridX + 1, checkY]);
                        }
                    }
                    if (moveNegative)
                    {
                        neighbouringNodes.Add(nodeArray[node.gridX - 1, node.gridY]);
                    }
                    if (movePositive)
                    {
                        neighbouringNodes.Add(nodeArray[node.gridX + 1, node.gridY]);
                    }
                }
            }

            return neighbouringNodes;
        }
        else
        {
            return GetNeighbouringNodes(node);
        }
    }

    public void UpdateGrid(Vector3 updatePoint, int updateDepth = 1)
    {
        Node updateNode = NodeFromWorldPoint(updatePoint);
        if (updateNode == null)
            return;

        int checkX;
        int checkY;

        for (int x = -updateDepth; x <= updateDepth; x++)
        {
            for (int y = -updateDepth; y <= updateDepth; y++)
            {
                checkX = updateNode.gridX + x;
                checkY = updateNode.gridY + y;

                Collider[] cast = new Collider[3];

                if (checkX >= 0 && checkX < gridSizeX)
                {
                    if (checkY >= 0 && checkY < gridSizeY)
                    {
                        Vector3 worldPosition = topRight + Vector3.left * (checkX * nodeDiameter + nodeRadius) + Vector3.down * (checkY * nodeDiameter + nodeRadius);
                        bool blocking = false;

                        int collided = Physics.OverlapSphereNonAlloc(worldPosition, nodeRadius * blockingFeather, cast, safeLayer);
                        if (collided > 0)
                        {
                            blocking = false;
                        }

                        nodeArray[checkX, checkY] = new Node(blocking, worldPosition, checkX, checkY);
                    }
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 0, gridWorldSize.y));

        if (nodeArray != null)
        {
            foreach (Node n in nodeArray)
            {
                if (n == null)
                    continue;

                Gizmos.color = walkable;
                if (n.isBlocker)
                {
                    Gizmos.color = blocked;
                }

                if (n.isSpaceNode)
                {
                    Gizmos.color = space;
                }

                Gizmos.DrawWireCube(n.position, Vector3.one * (nodeDiameter - spacing));
            }
        }
    }
}

public static class PathfindingGridExt
{
    public static PathfindingGrid.Direction Reverse(this PathfindingGrid.Direction direction)
    {
        if (direction == PathfindingGrid.Direction.Up)
        {
            return PathfindingGrid.Direction.Down;
        }
        else if (direction == PathfindingGrid.Direction.Down)
        {
            return PathfindingGrid.Direction.Up;
        }
        else if (direction == PathfindingGrid.Direction.Left)
        {
            return PathfindingGrid.Direction.Right;
        }
        else
        {
            return PathfindingGrid.Direction.Left;
        }
    }
}