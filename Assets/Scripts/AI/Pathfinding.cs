﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{

    #region Singleton
    public static Pathfinding instance;

    void Awake()
    {
        if (instance)
        {
            Debug.LogWarning("Another instance of pathfinding already exists");
            return;
        }

        instance = this;
    }

    #endregion

    PathfindingGrid grid;

    void Start()
    {
        grid = PathfindingGrid.instance;
    }

    public bool AStarPath(Vector3 startPoint, Vector3 destination)     // Pathfinding using A* algorithm
    {
        List<Node> OPEN = new List<Node>();             // Set of nodes to be evaluated
        HashSet<Node> CLOSED = new HashSet<Node>();     // Set of evaluated nodes

        Node startNode = grid.GetPathableNode(startPoint);
        Node targetNode = grid.GetPathableNode(destination);    // Find the start and destination nodes from world points        

        if (startNode == null || targetNode == null || targetNode.isBlocker)
        {
            return false;
        }

        startNode.isOpen = true;
        OPEN.Add(startNode);                    // Start by adding the starting node to the open set

        while (OPEN.Count > 0)                  // Loop until there are no more possibilities
        {
            Node currentNode = OPEN[0];

            foreach (Node n in OPEN)
            {
                if (n.fCost < currentNode.fCost || (n.fCost == currentNode.hCost && n.hCost < currentNode.hCost))
                    currentNode = n;            // Find the node in OPEN with the lowest f Cost
            }

            OPEN.Remove(currentNode);
            currentNode.isOpen = false;
            currentNode.isClosed = true;
            CLOSED.Add(currentNode);            // Remove the current node from OPEN and add it to CLOSED

            if (currentNode == targetNode)
            {
                foreach (Node n in CLOSED)
                    n.isClosed = false;

                foreach (Node n in OPEN)
                    n.isOpen = false;

                return true;                    // A path has been found
            }

            List<Node> neighbouringNodes = grid.GetNeighbouringNodes(currentNode);

            foreach (Node neighbour in neighbouringNodes)
            {
                if (!neighbour.isBlocker && !neighbour.isClosed)
                {
                    int gCost = currentNode.gCost + GetDistance(currentNode, neighbour);

                    if (!neighbour.isOpen || gCost < neighbour.gCost)           // If the new path is shorter or we've never looked at the node before
                    {
                        neighbour.gCost = gCost;                                // Set or update the gCost of the neighbour
                        neighbour.hCost = GetDistance(neighbour, targetNode);   // Set the hCost of the neighbour

                        neighbour.parentNode = currentNode;                     // Set the parent node of the neighbour

                        if (!neighbour.isOpen)
                        {
                            neighbour.isOpen = true;
                            OPEN.Add(neighbour);                                // Add neighbour to open if it's not there already
                        }

                    }
                }
            }
        }

        foreach (Node n in CLOSED)
            n.isClosed = false;

        Debug.Log("NO PATH");
        return false;                           // A path has not been found
    }

    public List<Node> GetPath(Vector3 startPoint, Vector3 destination)
    {
        List<Node> path = new List<Node>();

        Node startNode = grid.GetPathableNode(startPoint);
        Node targetNode = grid.GetPathableNode(destination);        // Find the start and destination nodes from world points

        Node currentNode = targetNode;                             // Start at the end node

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parentNode;                  // Traverse through the nodes parent until back at the start
        }

        path.Reverse();                                            // Reverse the list to get the path in the correct direction

        return path;
    }

    int GetDistance(Node a, Node b)
    {
        int deltaX = Mathf.Abs(a.gridX - b.gridX);
        int deltaY = Mathf.Abs(a.gridY - b.gridY);

        return (deltaX + deltaY);
    }
}