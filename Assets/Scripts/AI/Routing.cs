﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Routing))]
public class RoutingEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Routing routing = (Routing)target;
        if (GUILayout.Button("Add nodes"))
        {
            routing.CreateRoutes();
        }

        base.OnInspectorGUI();
    }
}
#endif
public class Routing : MonoBehaviour
{
    public static Routing Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public RoutingNode[] RoutingNodes;
    public List<NodeImageData> RoutingNodeImageData;

    public void CreateRoutes()
    {
        RoutingNodes = GetComponentsInChildren<RoutingNode>();
    }

    public void Start()
    {
        foreach (RoutingNode node in RoutingNodes)
        {
            Material m = RoutingNodeImageData.Where(x => x.NodeType == node.NodeType).FirstOrDefault()?.NodeMaterial;
            
            if (m != null)
                node.gameObject.GetComponent<MeshRenderer>().material = m;
        }
    }
}
