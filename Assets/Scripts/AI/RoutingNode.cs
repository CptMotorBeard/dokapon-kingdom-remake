﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

[System.Serializable]
public class NodeImageData
{
    public RoutingNode.RoutingNodeType NodeType;
    public Material NodeMaterial;
}

public class RoutingNode : MonoBehaviour
{
    public enum RoutingNodeType
    {
        BattleNode,
        CaveEntranceNode,
        FieldMagicNode,
        ItemsNode,        
        RedLootNode,
        BankNode,
        StoreNode,
        TownNode,
        CastleNode,
        DokaponCastleNode,
        MiscNode,
        UnavailableNode,
        DarklingNode,
        DarklingCorruptedNode
    }

    public bool CanMoveLeft = true;
    public bool CanMoveRight = true;
    public bool CanMoveUp = true;
    public bool CanMoveDown = true;

    public RoutingNodeType NodeType;

    private void OnDrawGizmos()
    {
        if (CanMoveLeft)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, transform.position + new Vector3(-5, 0, 0));
        }

        if (CanMoveRight)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(transform.position, transform.position + new Vector3(5, 0, 0));
        }

        if (CanMoveDown)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + new Vector3(0, 0, -5));
        }

        if (CanMoveUp)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + new Vector3(0, 0, 5));
        }
    }
}
