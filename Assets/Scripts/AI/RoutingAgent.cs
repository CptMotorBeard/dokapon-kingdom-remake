﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;

public class RoutingAgent : MonoBehaviour
{
    public RoutingNode TargetRoutingNode;
    public float Speed = 5.0f;
    public float Delta = 0.8f;

    bool bFollowingPath = false;
    bool bGoToNode = false;

    List<Node> mFollowingNodes;
    Node mCurrentTarget;

    public delegate void PathingCompleteDelegate();
    public PathingCompleteDelegate OnPathingComplete;

    private void FixedUpdate()
    {
        if (TargetRoutingNode == null)
            return;

        if (bGoToNode)
        {
            float y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, TargetRoutingNode.transform.position, Speed * Time.fixedDeltaTime);
            transform.position = new Vector3(transform.position.x, y, transform.position.z);

            if (Vector3.Distance(transform.position, TargetRoutingNode.transform.position) < Delta)
            {
                TargetRoutingNode = null;
                bGoToNode = false;

                OnPathingComplete?.Invoke();
            }

            return;
        }

        if (bFollowingPath && mFollowingNodes != null)
        {
            if (mCurrentTarget == null)
            {
                mCurrentTarget = mFollowingNodes[0];
                mFollowingNodes.RemoveAt(0);

                while (!mCurrentTarget.isSpaceNode && mFollowingNodes.Count > 0)
                {
                    mCurrentTarget = mFollowingNodes[0];
                    mFollowingNodes.RemoveAt(0);
                }                
            }

            float y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, mCurrentTarget.position, Speed * Time.fixedDeltaTime);
            transform.position = new Vector3(transform.position.x, y, transform.position.z);

            if (Vector3.Distance(transform.position, mCurrentTarget.position) < Delta)
            {
                mCurrentTarget = null;
                if (mFollowingNodes.Count <= 0)
                {
                    bFollowingPath = false;
                    bGoToNode = true;
                }
            }
        }
        else
        {
            Pathfinding p = Pathfinding.instance;
            if (p.AStarPath(transform.position, TargetRoutingNode.transform.position))
            {
                mFollowingNodes = p.GetPath(transform.position, TargetRoutingNode.transform.position);
                bFollowingPath = true;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (mCurrentTarget != null)
        {
            Gizmos.color = Color.yellow;
            if (mFollowingNodes != null && mFollowingNodes.Count > 0)
            {
                for (int i = 0; i < mFollowingNodes.Count - 1; i++)
                {
                    Node a = mFollowingNodes[i];
                    Node b = mFollowingNodes[i + 1];

                    Gizmos.DrawLine(a.position, b.position);
                }
            }

            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, mCurrentTarget.position);
        }
    }
}
