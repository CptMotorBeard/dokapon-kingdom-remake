﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TooManyItemsSelector : MonoBehaviour
{
    public Image ItemImage;
    public TextMeshProUGUI ItemName;

    public Button ItemButton;

    public void Initialize(Item item, Action<Item, bool> onItemClick, bool dispose=false)
    {
        if (item == null)
        {
            Debug.LogError("Initialized without an item");
            return;
        }

        ItemImage.sprite = item.ItemSprite;
        ItemName.text = item.Name;

        ItemButton.onClick.AddListener(delegate { onItemClick(item, dispose); });
    }
}
