﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SwapEquipment : MonoBehaviour
{
    public SwapEquipmentItemPanel CurrentItem;
    public SwapEquipmentItemPanel NewItem;

    public GameObject ShopSellDisplay;
    public TextMeshProUGUI ShopSellText;

    public Button YesConfirm;
    public Button NoConfirm;

    public delegate void ButtonClickedDelegate(bool shouldSwap);
    public ButtonClickedDelegate OnConfirmationClicked;

    public void Initialize(Equipment currentItem, Equipment newItem, bool isShop=false, float sellPrice=1)
    {
        if (isShop)
        {
            ShopSellDisplay.SetActive(true);

            int moneySaved = Mathf.FloorToInt(currentItem.Price * sellPrice);
            long newPrice = newItem.Price - moneySaved;

            ShopSellText.text = $"{newItem.Price}\t\t-\t\t{moneySaved}\t\t=\t\t{newPrice}";
        }

        CurrentItem.Initialize(currentItem);
        NewItem.Initialize(newItem);

        YesConfirm.onClick.AddListener(delegate { ConfirmationClicked(true); });
        NoConfirm.onClick.AddListener(delegate { ConfirmationClicked(false); });
    }

    void ConfirmationClicked(bool shouldSwap)
    {
        OnConfirmationClicked?.Invoke(shouldSwap);
        Destroy(gameObject);
    }
}
