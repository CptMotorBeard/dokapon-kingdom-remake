﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TownUiManager : MonoBehaviour
{
    public GameObject SidePanel;
    
    public TextMeshProUGUI GoldText;
    public TextMeshProUGUI TownNameText;
    public Image TownLevel;

    public Sprite[] TownLevelSprites;

    public void Initialize(Town town)
    {
        GoldText.text = $"{GlobalGameManager.Instance.GetCurrentPlayer()?.TotalGold} G";

        TownNameText.text = town.Stats.TownName;
        TownLevel.sprite = TownLevelSprites[town.TownLevel];
    }
}
