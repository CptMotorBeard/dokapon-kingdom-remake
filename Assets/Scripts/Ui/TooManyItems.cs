﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooManyItems : MonoBehaviour
{
    public delegate void InventoryClearDelegate(List<Item> itemsToDispose);
    public InventoryClearDelegate OnInventoryClear;

    public RectTransform InventoryParent;
    public RectTransform DisposeParent;

    public GameObject Selector;
    public Button ExitButton;

    int mTargetDisposeCount;

    List<Item> mItems;
    List<Item> mPlayerItems;
    List<Item> mItemsToDispose;

    Player mPlayer;

    ItemType? mItemType;

    private void Start()
    {
        ExitButton.interactable = false;
        ExitButton.onClick.AddListener(delegate { Finish(); });
    }

    public void Initialize(Player player, List<Item> items)
    {
        mPlayer = player;
        
        mItems = new List<Item>();
        mItemsToDispose = new List<Item>();

        mItemType = items?.First()?.Type;

        if (mItemType == null)
        {
            Debug.LogError("Initialized with a null item or list");
            return;
        }            

        if (mItemType == ItemType.Item)
        {
            mPlayerItems = mPlayer.ItemInventory.ToList();
        }
        else if (mItemType == ItemType.FieldMagic)
        {
            mPlayerItems = mPlayer.MagicInventory.ToList();
        }
        else
        {
            Debug.LogError("Initialized with an invalid item type");
            return;
        }

        foreach (var item in items)
        {
            if (item.Type == mItemType)
            {
                mItems.Add(item);
            }
            else
            {
                Debug.LogError($"Invalid item included: {item.Name}");
            }
        }

        mTargetDisposeCount = mItems.Count;
        InitializeInventory();
    }

    private void InitializeInventory()
    {
        foreach (var item in mItems)
        {
            GameObject g = Instantiate(Selector, InventoryParent);
            TooManyItemsSelector s = g.GetComponent<TooManyItemsSelector>();
            s.Initialize(item, OnClickItem);
        }

        foreach (var item in mPlayerItems)
        {
            GameObject g = Instantiate(Selector, InventoryParent);
            TooManyItemsSelector s = g.GetComponent<TooManyItemsSelector>();
            s.Initialize(item, OnClickItem);
        }

        foreach (var item in mItemsToDispose)
        {
            GameObject g = Instantiate(Selector, DisposeParent);
            TooManyItemsSelector s = g.GetComponent<TooManyItemsSelector>();
            s.Initialize(item, OnClickItem, true);
        }

        InventoryParent.sizeDelta = new Vector2(InventoryParent.sizeDelta.x, 100 * (mItems.Count + mPlayerItems.Count));
        DisposeParent.sizeDelta = new Vector2(DisposeParent.sizeDelta.x, 100 * mItemsToDispose.Count);
    }

    private void OnClickItem(Item item, bool disposeItem = false)
    {
        if (disposeItem)
        {
            if (mItemsToDispose.Contains(item))
            {
                mItemsToDispose.Remove(item);
                mItems.Add(item);
            }
            else
            {
                Debug.LogError("Clicked an item that doesn't exist");
                return;
            }
        }
        else
        {
            if (mItems.Contains(item))
            {
                mItems.Remove(item);
                mItemsToDispose.Add(item);
            }
            else if (mPlayerItems.Contains(item))
            {
                mPlayerItems.Remove(item);
                mItemsToDispose.Add(item);
            }
            else
            {
                Debug.LogError("Clicked an item that doesn't exist");
                return;
            }
        }
        
        RefreshUI();
    }

    private void RefreshUI()
    {
        for (int i = InventoryParent.childCount - 1; i >= 0; --i)
        {
            Destroy((InventoryParent.GetChild(i).gameObject));
        }
        
        for (int i = DisposeParent.childCount - 1; i >= 0; --i)
        {
            Destroy((DisposeParent.GetChild(i).gameObject));
        }

        InitializeInventory();

        if (mItemsToDispose.Count >= mTargetDisposeCount)
        {
            ExitButton.interactable = true;
        }
        else
        {
            ExitButton.interactable = false;
        }
    }

    public void Finish()
    {
        if (mItemType != null)
        {
            List<Item> allItems = mItems.Concat(mPlayerItems).ToList();
            mPlayer.ClearInventory(mItemType.Value);
            mPlayer.AddItems(allItems);

            OnInventoryClear?.Invoke(mItemsToDispose);
        }

        Destroy(gameObject);        
    }
}
