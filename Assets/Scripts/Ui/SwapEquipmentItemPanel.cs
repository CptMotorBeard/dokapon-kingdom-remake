﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SwapEquipmentItemPanel : MonoBehaviour
{
    public Image EquipmentSprite;
    public TextMeshProUGUI EquipmentName;

    public TextMeshProUGUI AT;
    public TextMeshProUGUI DF;
    public TextMeshProUGUI MG;
    public TextMeshProUGUI SP;
    public TextMeshProUGUI HP;

    public TextMeshProUGUI Description;

    public void Initialize(Equipment equipment)
    {
        EquipmentSprite.sprite = equipment.ItemSprite;
        EquipmentName.text = equipment.Name;

        AT.text = $"{equipment.Attack}";
        DF.text = $"{equipment.Defense}";
        MG.text = $"{equipment.Magic}";
        SP.text = $"{equipment.Speed}";
        HP.text = $"{equipment.Hp}";

        Description.text = equipment.Description;
    }
}
