﻿using UnityEngine;
using UnityEngine.UI;

public class TransitionController : MonoBehaviour
{
    public Image FadeImage;
    public Canvas ThisCanvas;

    public float FadeTime = 1.0f;

    public void FadeIn()
    {
        FadeImage.color = new Color(FadeImage.color.r, FadeImage.color.g, FadeImage.color.b, 1f);

        ThisCanvas.enabled = true;
        LeanTween.alpha(FadeImage.rectTransform, 0f, FadeTime).setEase(LeanTweenType.linear).setOnComplete(FadeFinish);
    }

    public void FadeOut()
    {
        FadeImage.color = new Color(FadeImage.color.r, FadeImage.color.g, FadeImage.color.b, 0f);

        ThisCanvas.enabled = true;
        LeanTween.alpha(FadeImage.rectTransform, 1f, FadeTime).setEase(LeanTweenType.linear);
    }

    private void FadeFinish()
    {
        ThisCanvas.enabled = false;
    }
}
