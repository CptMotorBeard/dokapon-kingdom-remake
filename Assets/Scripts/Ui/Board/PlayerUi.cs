﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.Jobs;

public class PlayerUi : MonoBehaviour
{
    public static PlayerUi Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    [Header("Panels")]
    public GameObject SidePanel;
    public GameObject PlayerPanel;
    public GameObject BottomPanel;
    public GameObject DatePanel;
    public GameObject InventoryPanel;
    public GameObject EnterBattleUiPanel;
    public GameObject ConfirmationPanel;

    [Header("Spinner")]
    public GameObject GameSpinner;
    public Transform GameSpinnerParent;
    private Spinner3D[] mGameSpinner;
    public Button SpinnerButton;
    public TextMeshProUGUI RemainingSpacesText;
    public GameObject RemainingSpacesObject;

    [Header("Item Spinner")]
    public ItemSpinner ItemSpinnerUi;

    [Header("Battle")]
    public EnterBattleUI BattleUi;

    [Header("Player Movement Confirmation")]    
    public Button ConfirmationYesButton;
    public Button ConfirmationNoButton;

    [Header("Death")]
    public GameObject DeathUi;
    public Button DeathConfirmationButton;
    public TextMeshProUGUI DeathConfirmationText;

    #region Date
    [Header("Date")]
    public Sprite[] DateSprites;
    public Image DateImage;
    public TextMeshProUGUI WeekText;
    #endregion

    #region Player
    [Header("Player")]
    public TextMeshProUGUI PlayerName;
    public TextMeshProUGUI Level;
    public TextMeshProUGUI Attack;
    public TextMeshProUGUI Defense;
    public TextMeshProUGUI Magic;
    public TextMeshProUGUI Speed;
    public TextMeshProUGUI CurHp;
    public TextMeshProUGUI MaxHp;
    public TextMeshProUGUI Money;

    public Slider HealthSlider;

    public Image Position;
    public Sprite[] AllPositions;
    #endregion

    #region OtherPlayers
    [Header("Other Players")]
    public OtherPlayerUi[] OtherPlayersUi;
    #endregion

    private int mNumberOfSpaces = 0;
    GlobalGameManager mGlobalGameManager;
    GameFlowManager mGameFlowManager;

    int mNumberOfSpinners = 1;

    public void Initialize()
    {
        mGlobalGameManager = GlobalGameManager.Instance;
        mGameFlowManager = mGlobalGameManager.FlowManager;

        RemainingSpacesObject.SetActive(false);
        
        SidePanel.SetActive(true);
        PlayerPanel.SetActive(true);
        BottomPanel.SetActive(true);
        DatePanel.SetActive(true);
        InventoryPanel.SetActive(false);
        EnterBattleUiPanel.SetActive(false);
        DeathUi.SetActive(false);

        ItemSpinnerUi.gameObject.SetActive(false);

        SpinnerButton.onClick.AddListener(delegate { StartSpinner(); });

        SetupDate();
        SetupCurrentPlayer();
        SetupOtherPlayers();        
    }

    private void SetupDate()
    {
        WeekText.text = $"{mGlobalGameManager.Week}";
        DateImage.sprite = DateSprites[mGlobalGameManager.Date];
    }

    private void SetupCurrentPlayer()
    {
        Player player = mGlobalGameManager.GetCurrentPlayer();
        if (player == null)
        {
            Debug.LogError("Current player was null");
            return;
        }

        PlayerName.text = player.CharacterName;
        Level.text = $"{player.Level}";

        Attack.text = $"{player.CharacterStats[CharacterStat.Attack]}";
        Defense.text = $"{player.CharacterStats[CharacterStat.Defense]}";
        Magic.text = $"{player.CharacterStats[CharacterStat.Magic]}";
        Speed.text = $"{player.CharacterStats[CharacterStat.Speed]}";

        CurHp.text = $"{player.CurHp} /";
        MaxHp.text = $"{player.CharacterStats[CharacterStat.Hp]}";

        Money.text = $"{player.TotalGold} G";

        int ranking = mGlobalGameManager.GetCurrentPlayerRanking();
        Position.sprite = AllPositions[ranking];

        HealthSlider.value = player.CurHp / (float)player.CharacterStats[CharacterStat.Hp];

        player.OnPlayerMovementSet = null;
        player.OnPlayerSpinnerSet = null;

        player.OnPlayerSpinnerSet += () => { mNumberOfSpinners = player.NumberOfSpinners; };
        player.OnPlayerMovementSet += () => { CrystalUsed(player.ForcedMovementSpaces); };
    }

    private void SetupOtherPlayers()
    {
        Player[] otherPlayers = mGlobalGameManager.GetOtherPlayers();

        for (int i = 0; i < mGlobalGameManager.NumberOfPlayers - 1; i++)
        {
            OtherPlayersUi[i].gameObject.SetActive(true);
            OtherPlayersUi[i].Initialize(otherPlayers[i]);
        }

        for (int i = mGlobalGameManager.NumberOfPlayers - 1; i < OtherPlayersUi.Length; i++)
        {
            OtherPlayersUi[i].gameObject.SetActive(false);
        }
    }

    public void StartSpinner()
    {
        SidePanel.SetActive(false);
        PlayerPanel.SetActive(false);
        BottomPanel.SetActive(false);
        DatePanel.SetActive(false);

        mGameSpinner = new Spinner3D[mNumberOfSpinners];
        mNumberOfSpaces = 0;

        for (int i = 0; i < mNumberOfSpinners; i++)
        {
            int spinnerValue = Random.Range(0, 7);
            mNumberOfSpaces += spinnerValue;

            mGameSpinner[i] = Instantiate(GameSpinner).GetComponent<Spinner3D>();
            mGameSpinner[i].transform.SetParent(GameSpinnerParent);
            mGameSpinner[i].OnSpinnerStop += SpinnerStop;
            mGameSpinner[i].StartSpinner(spinnerValue, i, mNumberOfSpinners);
        }

        SpinnerButton.onClick.RemoveAllListeners();
    }

    private int mTotalSpinnersStopped = 0;
    private void SpinnerStop(int index)
    {
        mTotalSpinnersStopped++;
        mGameSpinner[index].OnSpinnerStop -= SpinnerStop;
        
        if (mTotalSpinnersStopped == mGameSpinner.Length)
        {
            foreach (Spinner3D s in mGameSpinner)
            {
                Destroy(s.gameObject);
            }

            mTotalSpinnersStopped = 0;
            RemainingSpacesObject.SetActive(true);

            UpdateSpaces(mNumberOfSpaces);
            mGameFlowManager.SpinnerStop(mNumberOfSpaces);
        }        
    }

    public void CrystalUsed(int numSpaces)
    {
        mNumberOfSpaces = numSpaces;

        SidePanel.SetActive(false);
        PlayerPanel.SetActive(false);
        BottomPanel.SetActive(false);
        DatePanel.SetActive(false);
        InventoryPanel.SetActive(false);
        RemainingSpacesObject.SetActive(true);

        UpdateSpaces(mNumberOfSpaces);
        mGameFlowManager.SpinnerStop(mNumberOfSpaces);
    }

    public void UpdateSpaces(int newSpaces)
    {
        mNumberOfSpaces = newSpaces;
        RemainingSpacesText.text = $"Remaining Spaces: {mNumberOfSpaces}";
    }

    public delegate void DeathConfirmationDelegate();
    public DeathConfirmationDelegate OnDeathConfirmed;
    public void DisplayDeathTimer(string characterName, int timer)
    {
        string deathString = $"{characterName} is dead for {timer} more turns";

        SidePanel.SetActive(false);
        PlayerPanel.SetActive(false);
        BottomPanel.SetActive(false);
        DatePanel.SetActive(false);
        InventoryPanel.SetActive(false);
        DeathUi.SetActive(true);
        DeathConfirmationButton.onClick.AddListener(delegate { OnDeathConfirmed?.Invoke(); });
        DeathConfirmationText.text = deathString;
    }

    public delegate void TurnEndConfirmationDelegate(bool turnEnd, RoutingNode endNode);
    public TurnEndConfirmationDelegate OnTurnEndConfirmation;
    public void DisplayEndTurnConfirmation(RoutingNode endNode)
    {
        SidePanel.SetActive(false);
        PlayerPanel.SetActive(false);
        BottomPanel.SetActive(false);
        DatePanel.SetActive(false);
        ConfirmationPanel.SetActive(true);

        ConfirmationYesButton.onClick.AddListener(delegate { TurnEndConfirmationClicked(true, endNode); });
        ConfirmationNoButton.onClick.AddListener(delegate { TurnEndConfirmationClicked(false, endNode); });
    }

    private void TurnEndConfirmationClicked(bool turnEnd, RoutingNode endNode)
    {
        ConfirmationPanel.SetActive(false);

        ConfirmationYesButton.onClick.RemoveAllListeners();
        ConfirmationNoButton.onClick.RemoveAllListeners();

        OnTurnEndConfirmation?.Invoke(turnEnd, endNode);
    }

    private void SwitchToBattlePanel()
    {
        SidePanel.SetActive(false);
        PlayerPanel.SetActive(false);
        BottomPanel.SetActive(false);
        DatePanel.SetActive(false);
        InventoryPanel.SetActive(false);
        EnterBattleUiPanel.SetActive(true);
    }

    public IEnumerator StartBattle(EnemyStats enemy)
    {
        SwitchToBattlePanel();
        BattleUi.Initialize(enemy);

        yield return BattleUi.Animate();
    }

    public IEnumerator StartBattle(Character enemy)
    {
        SwitchToBattlePanel();
        BattleUi.Initialize(enemy);

        yield return BattleUi.Animate();
    }

    public IEnumerator StartBattle(int index)
    {
        SwitchToBattlePanel();

        if (mGlobalGameManager == null)
            mGlobalGameManager = GlobalGameManager.Instance;

        Player otherPlayer = mGlobalGameManager.GetPlayer(index);
        BattleUi.Initialize(otherPlayer);

        yield return BattleUi.Animate();
    }

    public IEnumerator StartItemSpinner(List<Item> possibleDrops, Item dropSelected)
    {
        ItemSpinnerUi.gameObject.SetActive(true);

        int itemIndex = possibleDrops.IndexOf(dropSelected);

        ItemSpinnerUi.InitializeSpinner(possibleDrops, itemIndex);
        yield return ItemSpinnerUi.Spin();
    }
}
