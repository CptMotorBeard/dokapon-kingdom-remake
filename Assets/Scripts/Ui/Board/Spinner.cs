﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public delegate void SpinnerStopDelegate(int index);
    public SpinnerStopDelegate OnSpinnerStop;

    public RectTransform SpinnerArrow;
    public GameObject SpinnerObject;

    int mSpinnerIndex;
    int mTargetNumber = 0;
    float mTargetRotation = 0;

    public void StartSpinner(int targetNumber, int spinnerIndex)
    {
        mSpinnerIndex = spinnerIndex;

        StartCoroutine(Spin(targetNumber));
    }

    private IEnumerator Spin(int targetNumber)
    {
        float spinnerDegreesPerFrame = 50;

        while (!Input.GetMouseButton(0))
        {
            Vector3 curRotation = SpinnerArrow.localRotation.eulerAngles;
            float zRotation = ((curRotation.z + 360) - spinnerDegreesPerFrame) % 360;

            SpinnerArrow.localRotation = Quaternion.Euler(curRotation.x, curRotation.y, zRotation);

            yield return new WaitForSeconds(1000f / 30 / 1000);
        }

        yield return StopSpinning(targetNumber);
    }

    private IEnumerator StopSpinning(int targetNumber)
    {
        float slowDownDegrees = 145;
        float spinnerDegreesPerFrame = 50;

        mTargetNumber = targetNumber;
        mTargetRotation = TargetRotationForNumber(mTargetNumber);

        Vector3 curRotation = SpinnerArrow.localRotation.eulerAngles;
        float target = ((mTargetRotation + 360) + slowDownDegrees) % 360;

        while (curRotation.z < (target - 10) || curRotation.z > (target + 10))
        {
            float zRotation = ((curRotation.z + 360) - spinnerDegreesPerFrame) % 360;
            SpinnerArrow.localRotation = Quaternion.Euler(curRotation.x, curRotation.y, zRotation);
            curRotation = SpinnerArrow.localRotation.eulerAngles;

            yield return new WaitForSeconds(1000f / 30 / 1000);
        }        

        float degrees = spinnerDegreesPerFrame;

        while (degrees > 0)
        {
            degrees -= 1;

            float zRotation = ((curRotation.z + 360) - degrees) % 360;

            SpinnerArrow.localRotation = Quaternion.Euler(curRotation.x, curRotation.y, zRotation);
            curRotation = SpinnerArrow.localRotation.eulerAngles;

            yield return new WaitForSeconds(1000f / 30 / 1000);
        }

        yield return DelayStopSpinner();
    }

    IEnumerator DelayStopSpinner()
    {
        yield return new WaitForSeconds(1.0f);
        OnSpinnerStop?.Invoke(mSpinnerIndex);
    }

    float TargetRotationForNumber(int number)
    {
        switch (number)
        {
            case 0:
                return 0;
            case 1:
                return 320;
            case 2:
                return 270;
            case 3:
                return 205;
            case 4:
                return 150;
            case 5:
                return 94;
            case 6:
                return 40;
            default:
                return 0;
        }
    }
}
