﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnterBattleUI : MonoBehaviour
{
    public Animator BattleEnterAnimator;
    public float AnimationTime;
    public Sprite EnemyIcon;

    [Header("Left Player")]
    public TextMeshProUGUI LeftPlayerName;
    public TextMeshProUGUI LeftPlayerLevel;
    public Slider LeftPlayerHealthBar;
    public TextMeshProUGUI LeftPlayerMaxHp;
    public TextMeshProUGUI LeftPlayerCurHp;
    public Image LeftPlayerIcon;

    [Header("Right Player")]
    public TextMeshProUGUI RightPlayerName;
    public TextMeshProUGUI RightPlayerLevel;
    public Slider RightPlayerHealthBar;
    public TextMeshProUGUI RightPlayerMaxHp;
    public TextMeshProUGUI RightPlayerCurHp;
    public Image RightPlayerIcon;

    private void InitializePlayer()
    {
        Player p = GlobalGameManager.Instance.GetCurrentPlayer();
        if (p == null)
        {
            Debug.LogError("Current player was null");
            return;
        }

        LeftPlayerName.text = p.CharacterName;
        LeftPlayerLevel.text = $"{p.Level}";

        float curHp = p.CurHp;
        float maxHp = p.CharacterStats[CharacterStat.Hp];

        LeftPlayerHealthBar.value = curHp / maxHp;
        LeftPlayerCurHp.text = $"{curHp} /";
        LeftPlayerMaxHp.text = $"{maxHp}";

        // LeftPlayerIcon.Sprite = p.CharacterIcon;
    }

    public void Initialize(EnemyStats enemy)
    {
        InitializePlayer();

        RightPlayerName.text = enemy.CharacterName;
        RightPlayerLevel.text = $"{enemy.Level}";

        RightPlayerHealthBar.value = 1;
        RightPlayerCurHp.text = $"{enemy.MaxHp} /";
        RightPlayerMaxHp.text = $"{enemy.MaxHp}";

        RightPlayerIcon.sprite = EnemyIcon;
    }

    public void Initialize(Character otherCharacter)
    {
        if (otherCharacter == null)
        {
            Debug.LogError("Initializing battle ui without the other character");
            return;
        }
        InitializePlayer();

        RightPlayerName.text = otherCharacter.CharacterName;
        RightPlayerLevel.text = $"{otherCharacter.Level}";

        float curHp = otherCharacter.CurHp;
        float maxHp = otherCharacter.CharacterStats[CharacterStat.Hp];

        RightPlayerHealthBar.value = curHp / maxHp;
        RightPlayerCurHp.text = $"{curHp} /";
        RightPlayerMaxHp.text = $"{maxHp}";

        // RightPlayerIcon.Sprite = otherPlayer.CharacterIcon;
    }

    public IEnumerator Animate()
    {
        BattleEnterAnimator.SetTrigger("Start");
        yield return new WaitForSeconds(AnimationTime);
    }
}
