﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemSpinner : MonoBehaviour
{
    public GameObject NewItemObject;
    public TextMeshProUGUI NewItemText;

    public Color LitColour;
    public Color DimColour;

    public TMP_FontAsset TextFont;

    public float Radius;
    public float StartingAngle;
    public float Spacing;

    public Sprite SpinnerItemSprite;

    List<Image> mSpinnerSpaces;
    int mCurrentItem = 0;

    private int mItemsCount = 6;
    private int mTargetIndex = 0;
    private Item mWinningItem;

    public void InitializeSpinner(List<Item> items, int targetItemIndex)
    {
        mItemsCount = items.Count;

        if (targetItemIndex < 0 || targetItemIndex > mItemsCount)
        {
            Debug.LogError("Initializing a spinner with an invalid reward");
            return;
        }

        mTargetIndex = targetItemIndex;
        mWinningItem = items[mTargetIndex];

        mSpinnerSpaces = new List<Image>();

        for (int i = 0; i < mItemsCount; i++)
        {
            GameObject go = new GameObject();
            go.transform.parent = transform;
            go.transform.localPosition = Vector3.zero;
            go.AddComponent<CanvasRenderer>();

            Image im = go.AddComponent<Image>();
            im.sprite = SpinnerItemSprite;
            im.color = DimColour;

            float mod = Screen.width / 1920f;

            GameObject child = new GameObject();
            child.transform.parent = go.transform;
            child.transform.localPosition = new Vector2(-150 * mod, 0);

            Image itemSprite = child.AddComponent<Image>();
            itemSprite.sprite = items[i].ItemSprite;

            child = new GameObject();
            child.transform.parent = go.transform;
            child.transform.localPosition = new Vector2(25 * mod, 0);

            TextMeshProUGUI text = child.AddComponent<TextMeshProUGUI>();
            text.enableAutoSizing = true;
            text.fontSizeMin = 12;
            text.fontSizeMax = 100;
            text.font = TextFont;
            text.color = Color.black;
            text.alignment = TextAlignmentOptions.MidlineLeft;
            text.text = items[i].Name;

            itemSprite.rectTransform.sizeDelta = new Vector2(60 * mod, 60 * mod);
            text.rectTransform.sizeDelta = new Vector2(270 * mod, 60 * mod);
            im.rectTransform.sizeDelta = new Vector2(400 * mod, 100 * mod);

            mSpinnerSpaces.Add(im);
        }

        mSpinnerSpaces[mCurrentItem].color = LitColour;
        SetSpinnerAngles();
    }

    private void SetSpinnerAngles()
    {
        float angle = 360f / mItemsCount / 2f;
        float radAngle = angle * Mathf.Deg2Rad;
        float x = Radius * Mathf.Sin(radAngle);
        float y = Radius * Mathf.Cos(radAngle);

        float angleIncrease = 360f / mSpinnerSpaces.Count;

        foreach (Image i in mSpinnerSpaces)
        {
            if (!(angle == 180 || angle == 0))
            {
                if (angle > 0 && angle < 180)
                    x += (mItemsCount / 6f) * Spacing;
                else
                    x -= (mItemsCount / 6f) * Spacing;
            }
            else
            {
                if (angle == 180)
                    y -= Spacing;
                else
                    y += Spacing;
            }

            i.transform.localPosition = new Vector2(x, y);

            angle += angleIncrease;
            radAngle = angle * Mathf.Deg2Rad;

            x = Radius * Mathf.Sin(radAngle);
            y = Radius * Mathf.Cos(radAngle);
        }
    }

    private void MoveSpinnerForward()
    {
        mSpinnerSpaces[mCurrentItem].color = DimColour;

        mCurrentItem++;
        mCurrentItem %= mItemsCount;

        mSpinnerSpaces[mCurrentItem].color = LitColour;
    }

    public IEnumerator Spin()
    {
        float spinnerSpeed = 1000f / 30 / 1000;

        while (!(Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space)))
        {
            MoveSpinnerForward();
            yield return new WaitForSeconds(spinnerSpeed);
        }

        while (Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space))
        {
            MoveSpinnerForward();
            yield return new WaitForSeconds(spinnerSpeed);
        }

        int spinnerSlowIndex = (mTargetIndex + 1) % mItemsCount;
        while (mCurrentItem != spinnerSlowIndex)
        {
            MoveSpinnerForward();
            yield return new WaitForSeconds(spinnerSpeed);
        }

        int spinnerCount = 2;
        while (mCurrentItem != mTargetIndex)
        {
            MoveSpinnerForward();

            float wait = spinnerSpeed * spinnerCount;
            spinnerCount += 2;
            yield return new WaitForSeconds(wait);
        }

        yield return DisplayRewards();
    }

    public IEnumerator DisplayRewards()
    {
        mSpinnerSpaces.Clear();
        mSpinnerSpaces = null;

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        NewItemObject.SetActive(true);
        NewItemText.text = mWinningItem.Name;

        yield return WaitForClick();
        NewItemObject.SetActive(false);
    }

    // Input.GetMouseButtonDown doesn't seem to work in a coroutine, so gotta do some weird stuff for clicks
    public IEnumerator WaitForClick()
    {
        while (!Input.GetMouseButton(0))
        {
            yield return new WaitForEndOfFrame();
        }

        while (Input.GetMouseButton(0))
        {
            yield return new WaitForEndOfFrame();
        }
    }

}
