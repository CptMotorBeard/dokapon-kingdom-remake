﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TownDisplayPanel : MonoBehaviour
{
    public TextMeshProUGUI TownName;
    public Image DisplayBacker;

    public void Initialize(Town town)
    {
        if (town == null)
        {
            Debug.LogError("Initializing with null town");
            return;
        }

        TownName.name = town.Stats.TownName;

        if (town.Owner != null)
        {
            Color c = GlobalGameManager.Instance.GetPlayerColor(town.Owner);
            DisplayBacker.color = c;
        }
    }
}
