﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class PlayerInformationUI : MonoBehaviour
{
    public void QuitGame()
    {
        GlobalGameManager.Instance.QuitGame();
    }
}
