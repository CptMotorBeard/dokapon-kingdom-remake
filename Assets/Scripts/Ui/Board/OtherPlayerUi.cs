﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OtherPlayerUi : MonoBehaviour
{
    public TextMeshProUGUI CharacterName;
    public TextMeshProUGUI Level;
    public Slider HealthBarSlider;

    public void Initialize(Player player)
    {
        Level.text = $"{player.Level}";
        CharacterName.text = $"{player.CharacterName}";

        HealthBarSlider.value = player.CurHp / (float)player.CharacterStats[CharacterStat.Hp];
    }
}
