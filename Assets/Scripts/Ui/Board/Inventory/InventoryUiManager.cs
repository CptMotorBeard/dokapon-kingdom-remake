﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryUiManager : MonoBehaviour
{
    public GameObject ItemUi;
    public TextMeshProUGUI DescriptionField;

    public RectTransform InventoryParent;
    public RectTransform Content;
    public float InventoryItemHeight;

    private Player mPlayer;

    const string kMagicInventory = "magic";
    const string kItemInventory = "item";

    string mCurrentInventory;

    private void OnEnable()
    {
        mPlayer = GlobalGameManager.Instance.GetCurrentPlayer();
        InitializeInventory(mPlayer?.Job.ItemInventorySize ?? 0, mPlayer?.ItemInventory ?? new List<Item>());

        mCurrentInventory = kItemInventory;
    }

    public void OnItemButtonClick()
    {
        InitializeInventory(mPlayer?.Job.ItemInventorySize ?? 0, mPlayer?.ItemInventory ?? new List<Item>());

        mCurrentInventory = kItemInventory;
    }

    public void OnMagicButtonClick()
    {
        InitializeInventory(mPlayer?.Job.MagicInventorySize ?? 0, mPlayer?.MagicInventory ?? new List<Item>());

        mCurrentInventory = kMagicInventory;
    }

    private void InitializeInventory(float inventorySize, IReadOnlyList<Item> inventoryItems)
    {
        foreach (Transform child in InventoryParent)
        {
            Destroy(child.gameObject);
        }

        Content.sizeDelta = new Vector2(Content.rect.width, (InventoryItemHeight * inventorySize) / 2);

        foreach (Item item in inventoryItems)
        {
            GameObject ui = Instantiate(ItemUi);
            ui.transform.SetParent(InventoryParent);
            ui.transform.localScale = Vector3.one;

            InventoryUiItem inventoryUiItem = ui.GetComponent<InventoryUiItem>();
            inventoryUiItem.Initialize(item, DescriptionField, UseItemAndRefreshUI);
        }

        for (int i = inventoryItems.Count; i < inventorySize; i++)
        {
            GameObject ui = Instantiate(ItemUi);
            ui.transform.SetParent(InventoryParent);
            ui.transform.localScale = Vector3.one;

            InventoryUiItem inventoryUiItem = ui.GetComponent<InventoryUiItem>();
            inventoryUiItem.Initialize();
        }
    }

    private void UseItemAndRefreshUI(Item item)
    {
        if (item == null)
            return;

        item.GetAsConsumable()?.UseItem(mPlayer);

        if (mCurrentInventory == kItemInventory)
        {
            OnItemButtonClick();
        }
        else if (mCurrentInventory == kMagicInventory)
        {
            OnMagicButtonClick();
        }
    }
}
