﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System;

public class InventoryUiItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public Image ItemIcon;
    public TextMeshProUGUI ItemNameArea;

    private Item mItem;
    private TextMeshProUGUI mDescriptionText;

    private Action<Item> mOnClick;

    public void Initialize()
    {
        ItemIcon.enabled = false;
        ItemNameArea.enabled = false;
    }

    public void Initialize(Item item, TextMeshProUGUI descriptionText, Action<Item> onClick)
    {
        mOnClick = onClick;

        mItem = item;
        mDescriptionText = descriptionText;

        ItemIcon.sprite = mItem.ItemSprite;
        ItemNameArea.text = mItem.Name;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        mOnClick(mItem);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (mDescriptionText != null)
        {
            mDescriptionText.text = mItem.Description;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (mDescriptionText != null)
        {
            mDescriptionText.text = "";
        }
    }
}
