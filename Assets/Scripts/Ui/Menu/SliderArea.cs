﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;

public class SliderArea : MonoBehaviour
{
    public RectTransform Handle;
    public RectTransform HandleArea;

    float mWidth;
    float mHeight;
    bool mMouseDown = false;

    public delegate void ValueChangedDelegate();
    public ValueChangedDelegate OnValueChanged;

    public Vector2 Value
    {
        get
        {
            float x = Handle.localPosition.x / mWidth;
            float y = Handle.localPosition.y / mHeight;

            return new Vector2(x, y);
        }
    }

    private void Start()
    {
        mWidth = HandleArea.rect.width;
        mHeight = HandleArea.rect.height;
    }

    public void Initialize()
    {
        Handle.localPosition = new Vector2(mWidth, mHeight);
    }

    public void MouseDown(bool val)
    {
        mMouseDown = val;
    }

    private void Update()
    {
        if (mMouseDown)
        {
            Vector2 curValue;
            Vector2 prevValue;

            prevValue = Value;
            Handle.position = Input.mousePosition;

            if (Handle.localPosition.x < 0)
                Handle.localPosition = new Vector2(0, Handle.localPosition.y);
            else if (Handle.localPosition.x > mWidth)
                Handle.localPosition = new Vector2(mWidth, Handle.localPosition.y);

            if (Handle.localPosition.y < 0)
                Handle.localPosition = new Vector2(Handle.localPosition.x, 0);
            else if (Handle.localPosition.y > mHeight)
                Handle.localPosition = new Vector2(Handle.localPosition.x, mHeight);

            curValue = Value;
            if (curValue.x != prevValue.x || curValue.y != prevValue.y)
            {
                OnValueChanged?.Invoke();
            }
        }
    }
}
