﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;

public class ColourSelector : MonoBehaviour
{
    public SliderArea SaturationSlider;
    public Slider HueSlider;

    public Image DisplayColour;
    public Image SVDisplayColour;

    public Color Colour;

    public delegate void ValueChangedDelegate(Color newColour);
    public ValueChangedDelegate OnValueChanged;

    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        HueSlider.value = 0.5f;
        SaturationSlider.Initialize();

        UpdateColour();
        DisplayColour.color = Colour;
    }

    private void OnEnable()
    {
        SaturationSlider.OnValueChanged += UpdateColour;
        HueSlider.onValueChanged.AddListener(delegate { UpdateColour(); });
    }

    private void OnDisable()
    {
        SaturationSlider.OnValueChanged -= UpdateColour;
        HueSlider.onValueChanged.RemoveAllListeners();
    }

    void UpdateColour()
    {
        Vector2 saturationValue = SaturationSlider.Value;
        
        float hue = HueSlider.value;
        float saturation = saturationValue.x;
        float value = saturationValue.y;

        Color prevColour = Colour;
        Colour = Color.HSVToRGB(hue, saturation, value);

        SVDisplayColour.color = Color.HSVToRGB(hue, 1, 1);

        if (prevColour.r != Colour.r || prevColour.g != Colour.g || prevColour.b != Colour.b)
        {
            OnValueChanged?.Invoke(Colour);
            DisplayColour.color = Colour;
        }
    }
}
