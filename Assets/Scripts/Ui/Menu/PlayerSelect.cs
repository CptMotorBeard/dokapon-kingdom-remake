﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerSelect : MonoBehaviour
{
    public StartingPlayerJob[] AvailableStartingJobs;

    public GameObject UIObject;
    public GameObject ErrorMessage;
    public GameObject InputBlocker;

    public Canvas PreviousCanvas;
    public Canvas ThisCanvas;

    public GameObject[] PlayerSelections;

    private int mNumberOfPlayers = 1;
    private ColourSelector[] Colours;

    StartingPlayerJob[] mPlayerJobs;

    public void Initialize(float newPlayers)
    {
        mNumberOfPlayers = (int)newPlayers;
        Colours = new ColourSelector[mNumberOfPlayers];
        
        for (int i = 0; i < mNumberOfPlayers; i++)
        {
            PlayerSelections[i].SetActive(true);
            Colours[i] = PlayerSelections[i].GetComponentInChildren<ColourSelector>();
            Colours[i].Initialize();

            Button[] buttons = PlayerSelections[i].GetComponentsInChildren<Button>();
            foreach (Button b in buttons)
            {
                b.interactable = true;
            }
        }

        for (int i = mNumberOfPlayers; i < PlayerSelections.Length; i++)
        {
            PlayerSelections[i].SetActive(false);
        }

        mPlayerJobs = new StartingPlayerJob[mNumberOfPlayers];
    }

    public void Continue()
    {
        foreach (var j in mPlayerJobs)
        {
            if (j == null)
            {
                ErrorMessage.SetActive(true);
                ErrorMessage.GetComponentInChildren<TextMeshProUGUI>().text = "All players must have a class";
                return;
            }
        }

        GlobalGameManager ggm = GlobalGameManager.Instance;
        if (ggm != null)
        {
            string[] playerNames = new string[mNumberOfPlayers];
            for (int i = 0; i < mNumberOfPlayers; i++)
            {
                playerNames[i] = PlayerSelections[i].GetComponentInChildren<InputField>().text;
                if (string.IsNullOrEmpty(playerNames[i]))
                {
                    ErrorMessage.SetActive(true);
                    ErrorMessage.GetComponentInChildren<TextMeshProUGUI>().text = "All players must have a name";
                    return;
                }

                ggm.PlayerMaterial[i].color = Colours[i].Colour;
            }

            InputBlocker.SetActive(true);
            ggm.StartGame(mNumberOfPlayers, playerNames, mPlayerJobs);
        }        
    }

    public void GoBack()
    {
        PreviousCanvas.enabled = true;
        ThisCanvas.enabled = false;
    }

    public void SetThief(int playerIndex)
    {
        SetJob(playerIndex, 0);
    }

    public void SetMage(int playerIndex)
    {
        SetJob(playerIndex, 1);
    }
    public void SetWarrior(int playerIndex)
    {
        SetJob(playerIndex, 2);
    }

    private void SetJob(int playerIndex, int startingPlayerJob)
    {
        mPlayerJobs[playerIndex] = AvailableStartingJobs[startingPlayerJob];
    }
}
