﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NumberOfPlayers : MonoBehaviour
{
    public PlayerSelect PlayerSelection;
    public TextMeshProUGUI NumberOfPlayersText;
    public Slider NumberOfPlayersSlider;

    public Canvas ThisCanvas;
    public Canvas NextCanvas;

    float mNumberOfPlayers = 1;

    private void OnEnable()
    {
        NumberOfPlayersSlider.onValueChanged.AddListener(delegate { UpdatePlayers(); });
    }

    private void OnDisable()
    {
        NumberOfPlayersSlider.onValueChanged.RemoveAllListeners();
    }

    public void UpdatePlayers()
    {
        float newPlayers = NumberOfPlayersSlider.value;
        NumberOfPlayersText.text = $"Number of Players: {newPlayers}";

        mNumberOfPlayers = newPlayers;
    }

    public void Continue()
    {        
        PlayerSelection.Initialize(mNumberOfPlayers);
        ThisCanvas.enabled = false;
        NextCanvas.enabled = true;
    }
}
